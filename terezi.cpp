#include <boost/beast.hpp>

namespace beast = boost::beast;                 // from <boost/beast.hpp>
namespace http = beast::http;                   // from <boost/beast/http.hpp>
namespace websocket = beast::websocket;         // from <boost/beast/websocket.hpp>

#include <boost/asio.hpp>

namespace net = boost::asio;                    // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>

#include <unordered_set>
#include <fstream>

#include "work_distribution.h"
#include "search.h"

// Configuration constants
constexpr size_t MAX_PENDING_WORK = 3000;
constexpr size_t MAX_PENDING_WEIGHT = 100000000;
constexpr std::chrono::milliseconds WORKER_TIMEOUT{200000};
constexpr std::chrono::milliseconds WORKER_PING_INTERVAL{50000};

class AdminBroadcaster;
class AdminWebSocketSession;

// Global broadcaster instance
std::shared_ptr<AdminBroadcaster> g_admin_broadcaster;

void fail(beast::error_code ec, char const* what) {
  WARN << what << ": " << ec.message() << "\n";
}

class AdminBroadcaster {
private:
  std::mutex mutex_;
  std::unordered_set<std::shared_ptr<AdminWebSocketSession>> sessions_;
  
public:
  void add_session(std::shared_ptr<AdminWebSocketSession> session) {
    std::lock_guard lock(mutex_);
    sessions_.insert(session);
  }
  
  void remove_session(std::shared_ptr<AdminWebSocketSession> session) {
    std::lock_guard lock(mutex_);
    sessions_.erase(session);
  }
  
  void broadcast(const std::string& message);
};

// Global admin broadcast function
void adminConsoleBroadcast(const std::string& message) {
  if (g_admin_broadcaster) {
    g_admin_broadcaster->broadcast(message);
  }
}

class AdminWebSocketSession : public std::enable_shared_from_this<AdminWebSocketSession> {
public:
  AdminWebSocketSession(tcp::socket&& socket,
                       std::shared_ptr<AdminBroadcaster> broadcaster,
                       std::shared_ptr<WorkerStateManager> worker_state,
                       std::function<void()> connection_closed_cb)
    : ws_(std::move(socket))
    , broadcaster_(std::move(broadcaster))
    , worker_state_(std::move(worker_state))
    , connection_closed_cb_(std::move(connection_closed_cb)) {
  }

  ~AdminWebSocketSession() {
    // Remove session from broadcaster
    broadcaster_->remove_session(shared_from_this());
    
    // Close websocket
    // ws_.close(websocket::close_code::normal);
    connection_closed_cb_();
  }

  void run(http::request<http::string_body> req) {
    // Set suggested timeout settings for the websocket
    ws_.set_option(
      websocket::stream_base::timeout::suggested(
        beast::role_type::server));
    
    // Accept the websocket handshake
    ws_.async_accept(req, 
      beast::bind_front_handler(
        &AdminWebSocketSession::on_accept,
        shared_from_this()));
  }

  void send(std::shared_ptr<std::string const> const& s) {
    // Post our work to the strand, this ensures that the members of `this` will not be accessed concurrently.
    net::post(
      ws_.get_executor(),
      beast::bind_front_handler(
          &AdminWebSocketSession::on_send,
          shared_from_this(), s));
  }

  void send(std::string message) {
    auto const s = std::make_shared<std::string const>(std::move(message));
    send(s);
  }

private:
  websocket::stream<tcp::socket> ws_;
  beast::flat_buffer buffer_;
  std::shared_ptr<AdminBroadcaster> broadcaster_;
  std::shared_ptr<WorkerStateManager> worker_state_;
  std::function<void()> connection_closed_cb_;
  std::vector<std::shared_ptr<std::string const>> queue_;

  void fail(beast::error_code ec, char const* what) {
    if (ec == net::error::operation_aborted || ec == websocket::error::closed)
      return;
    std::cerr << what << ": " << ec.message() << "\n";
  }

  void on_send(std::shared_ptr<std::string const> const& s) {
    // Always add to queue
    queue_.push_back(s);

    // Are we already writing?
    if(queue_.size() > 1)
      return;

    // We are not currently writing, so send this immediately
    ws_.async_write(
      net::buffer(*queue_.front()),
      beast::bind_front_handler(
        &AdminWebSocketSession::on_write,
        shared_from_this()));
  }

  void on_accept(beast::error_code ec) {
    if (ec) {
      return fail(ec, "accept"); //do_close();
    }
    
    // Add session to broadcaster
    broadcaster_->add_session(shared_from_this());
    
    // Initialize admin console connection
    searchlogic::adminConsoleInitConn(
      [self = shared_from_this()](const std::pair<int, std::string>& s) {
        self->send(s.second);
      });
    
    // Start reading messages
    do_read();
  }

  void do_read() {
    ws_.async_read(
      buffer_,
      beast::bind_front_handler(
        &AdminWebSocketSession::on_read,
        shared_from_this()));
  }

  void on_read(beast::error_code ec, std::size_t bytes_transferred) {
    if (ec) {
      // Handle error
      return fail(ec, "read_ws");
    }
    
    // Process message
    std::string message = beast::buffers_to_string(buffer_.data());
    buffer_.consume(buffer_.size());

    if (message.empty()) {
      // Continue reading
      do_read();
      return;
    }

    handle_admin_command(message);

    // Continue reading
    do_read();
  }

  void handle_admin_command(const std::string& message) {
    // Handle admin command
    std::istringstream co(message);
    std::string command;
    co >> command;

    if (command == "help") {
      std::ostringstream res;
      res << "commands: connstats elevate ";
      res << searchlogic::adminConsoleCommands;
      send(res.str());
    } else if (command == "connstats") {
      std::ostringstream res;
      std::vector<uint64_t> uptimes = worker_state_->get_uptimes();
      res << uptimes.size() << " connections<br/>uptimes:";
      res << std::fixed << std::setprecision(1);
      for (auto i : uptimes) {
        res << " " << i / 60000.0 << "m";
      }
      send(res.str());
    } else if (command == "elevate") {
      int id;
      co >> id;
      worker_state_->elevate_worker(id);
      std::ostringstream res;
      res << "elevated connection " << id << " to permanent\n";
      send(res.str());
    } else {
      // Handle custom command
      searchlogic::adminConsoleCommandHandler(
        command, co,
        [self = shared_from_this()](const std::pair<int, std::string>& s) {
          self->send(s.second);
        });
    }
  }

  void on_write(beast::error_code ec, std::size_t bytes_transferred) {
    if (ec) {
      // Handle error
      return fail(ec, "write");
    }

    // Remove the string from the queue
    queue_.erase(queue_.begin());

    // Send the next message if any
    if(!queue_.empty())
      ws_.async_write(
        net::buffer(*queue_.front()),
        beast::bind_front_handler(
          &AdminWebSocketSession::on_write,
          shared_from_this()));
  }
};

void AdminBroadcaster::broadcast(const std::string& message) {
  // Put the message in a shared pointer so we can re-use it for each client
  auto const ss = std::make_shared<std::string const>(std::move(message));

  // Make a local list of all the weak pointers representing
  // the sessions, so we can do the actual sending without
  // holding the mutex:
  std::vector<std::weak_ptr<AdminWebSocketSession>> v;
  {
    std::lock_guard<std::mutex> lock(mutex_);
    v.reserve(sessions_.size());
    for(auto p : sessions_)
      v.emplace_back(p->weak_from_this());
  }

  // For each session in our local list, try to acquire a strong
  // pointer. If successful, then send the message on that session.
  for(auto const& weak : v)
    if(auto session = weak.lock())
      session->send(ss);
}

// Session class for handling HTTP requests

class HttpSession : public std::enable_shared_from_this<HttpSession> {
public:
  HttpSession(tcp::socket&& socket,
             std::shared_ptr<WorkDistributionQueue> pending_work,
             std::shared_ptr<WorkerStateManager> worker_state,
             std::shared_ptr<WorkReturnQueue> pending_return,
             std::function<void()> connection_closed_cb,
             size_t max_request_size = 64 * 1024 * 1024)
    : stream_(std::move(socket))
    , pending_work_(std::move(pending_work))
    , worker_state_(std::move(worker_state))
    , pending_return_(std::move(pending_return))
    , connection_closed_cb_(std::move(connection_closed_cb))
    , max_request_size_(max_request_size) {
  }

  void run() {
    // Set timeout
    stream_.expires_after(std::chrono::seconds(30));
    
    // Create a new parser with our custom size limits
    parser_.emplace();
    parser_->body_limit(max_request_size_);
    
    // Read the request
    http::async_read(stream_, buffer_, *parser_,
      beast::bind_front_handler(
        &HttpSession::on_read,
        shared_from_this()));
  }

private:
  beast::tcp_stream stream_;
  beast::flat_buffer buffer_;
  std::optional<http::request_parser<http::string_body>> parser_;
  http::response<http::string_body> res_;
  std::shared_ptr<WorkDistributionQueue> pending_work_;
  std::shared_ptr<WorkerStateManager> worker_state_;
  std::shared_ptr<WorkReturnQueue> pending_return_;
  std::function<void()> connection_closed_cb_;
  const size_t max_request_size_;

  // Parse getwork request (worker_id, max_weight)
  std::pair<uint64_t, int> parse_getwork_request(http::request<http::string_body>&& req) {
    std::istringstream co(req.body());
    uint64_t worker_id = 0;
    int max_weight = 0;
    co >> worker_id >> max_weight;
    
    if (co.fail() || max_weight < 0 || max_weight > 100000000) {
      throw std::runtime_error("Invalid getwork request");
    }

    if (!worker_state_->update_activity(worker_id)) {
      throw std::runtime_error("Invalid worker id");
    }
    return {worker_id, max_weight};
  }

  // Serialize work units in binary format
  std::string serialize_work_units(const std::vector<WorkUnit>& work_units) {
    std::ostringstream res(std::ios::out | std::ios::binary);
    
    // Write count
    writeInt(work_units.size(), res);
    
    // Write each work unit
    for (const auto& unit : work_units) {
      writeInt(unit.id, res);
      writeInt(unit.weight, res);
      writeString(unit.data, res);
    }
    
    return res.str();
  }

  void handle_getwork(http::request<http::string_body>&& req, 
                     http::response<http::string_body>& res) {
    try {
      auto [worker_id, max_weight] = parse_getwork_request(std::move(req));
      
      // Get work units
      auto work_units = pending_work_->try_pop(max_weight);
      
      // Update worker state
      for (const auto& unit : work_units) {
        worker_state_->assign_work(worker_id, unit.id);
      }
      
      // Serialize response
      res.body() = serialize_work_units(work_units);
      res.result(http::status::ok);
    } catch (const std::exception& e) {
      res.result(http::status::bad_request);
      res.body() = "-1";
    }
    res.prepare_payload();
  }
  
  struct CompletedWork {
    uint64_t worker_id;
    std::vector<WorkResult> units;
  };

  CompletedWork parse_returnwork_request(http::request<http::string_body>&& req) {
    std::istringstream co(req.body(), std::ios::in | std::ios::binary);
    CompletedWork result;
    
    int count = 0;
    readInt(count, co);
    readInt(result.worker_id, co);
    
    for (int i = 0; i < count; i++) {
      int id = 0, status = 0;
      std::string contributor, data;
      
      readInt(id, co);
      readInt(status, co);
      readString(contributor, co);
      
      if (status == 1) {
        readString(data, co);
        result.units.emplace_back(id, contributor, data);
      } else {
        result.units.emplace_back(id, contributor, ""); // Incomplete work
      }
      
      if (co.fail()) {
        throw std::runtime_error("Invalid returnwork format");
      }
    }
    
    return result;
  }

  void handle_returnwork(http::request<http::string_body>&& req,
                        http::response<http::string_body>& res) {
    try {
      // Validate size
      if (req.body().size() > max_request_size_) {
        throw std::runtime_error("Request too large");
      }
      
      // Parse and validate work units
      auto completed_work = parse_returnwork_request(std::move(req));

      if (!worker_state_->update_activity(completed_work.worker_id)) {
        throw std::runtime_error("Invalid worker id");
      }
      
      // Update worker state
      for (auto& unit : completed_work.units) {
        // Update worker state and mark work as completed
        worker_state_->complete_work(completed_work.worker_id, unit.id);
        pending_return_->push(std::move(unit));
        // if (unit.result.size()) {
        //   searchlogic::returnWork(unit.id, unit.result, unit.contributor);
        // } else {
        //   searchlogic::relinquishWork(unit.id);
        // }
      }
      
      res.result(http::status::ok);
      res.body() = "OK";
    } catch (const std::exception& e) {
      res.result(http::status::bad_request);
      res.body() = "FAIL";
    }
    res.prepare_payload();
  }

  uint64_t parse_worker_id(http::request<http::string_body>&& req) {
    std::istringstream co(req.body());
    uint64_t worker_id = 0;
    co >> worker_id;
    
    if (co.fail()) {
      throw std::runtime_error("Invalid worker ID");
    }
    return worker_id;
  }

  void handle_getuniqueid(http::request<http::string_body>&& req,
                         http::response<http::string_body>& res) {
    res.result(http::status::ok);
    res.body() = std::to_string(worker_state_->get_unique_worker_id());
    res.prepare_payload();
  }

  void on_read(beast::error_code ec, std::size_t bytes_transferred) {
    if (ec == http::error::end_of_stream) {
      return do_close();
    }
    if (ec) {
      fail(ec, "read_session");
      return do_close();
    }

    // Check for WebSocket upgrade
    if (websocket::is_upgrade(parser_->get())) {
      handle_websocket_upgrade();
      return;
    }

    // Handle regular HTTP request
    handle_http_request();
  }

  void handle_websocket_upgrade() {
    auto req = parser_->release();
    
    if (req.target() == "/admin-websocket") {
      auto session = std::make_shared<AdminWebSocketSession>(
        stream_.release_socket(),
        g_admin_broadcaster,
        worker_state_,
        connection_closed_cb_);
      session->run(std::move(req));
      return;
    }
    
    // Reject other WebSocket attempts
    res_ = {http::status::bad_request, req.version()};
    res_.body() = "WebSocket upgrade not allowed";
    res_.prepare_payload();
    http::async_write(stream_, res_,
      beast::bind_front_handler(
        &HttpSession::on_write,
        shared_from_this()));
  }

  void handle_http_request() {
    auto req = parser_->release();
    res_ = {http::status::ok, req.version()};
    res_.set(http::field::server, "Beast");
    
    try {
      if (req.method() == http::verb::post) {
        if (req.target() == "/getwork2") {
          handle_getwork(std::move(req), res_);
        } else if (req.target() == "/returnwork") {
          handle_returnwork(std::move(req), res_);
        } else if (req.target() == "/trickle") {
          handle_trickle(std::move(req), res_);
        } else {
          res_.result(http::status::not_found);
          res_.body() = "Unknown endpoint";
        }
      } else if (req.method() == http::verb::get) {
        if (req.target() == "/") {
          serve_static_file(res_, "index.html");
        } else if (req.target() == "/admin") {
          serve_static_file(res_, "admin.html");
        } else if (req.target() == "/getuniqueid") {
          handle_getuniqueid(std::move(req), res_);
        } else {
          res_.result(http::status::not_found);
          res_.body() = "Unknown endpoint";
        }
      } else {
        res_.result(http::status::method_not_allowed);
        res_.body() = "Invalid HTTP method";
      }
    } catch (const std::exception& e) {
      res_.result(http::status::internal_server_error);
      res_.body() = "Internal server error";
    }
    
    // Send the response
    res_.prepare_payload();
    http::async_write(stream_, res_,
      beast::bind_front_handler(
        &HttpSession::on_write,
        shared_from_this()));
  }

  void on_write(beast::error_code ec, std::size_t bytes_transferred) {
    if (ec) {
      // Handle error
      fail(ec, "write");
      return do_close();
    }
    
    // Read another request
    run();
  }

  void do_close() {
    beast::error_code ec;
    stream_.socket().shutdown(tcp::socket::shutdown_send, ec);
    connection_closed_cb_();
  }

  void serve_static_file(http::response<http::string_body>& res,
                        const std::string& path,
                        const std::string& content_type = "text/html") {
    beast::error_code ec;
    http::file_body::value_type file;
    
    // Attempt to open the file
    file.open(path.c_str(), beast::file_mode::scan, ec);
    
    // Handle file not found
    if (ec == beast::errc::no_such_file_or_directory) {
      res.result(http::status::not_found);
      res.body() = "File not found";
      return;
    }
    
    // Handle other errors
    if (ec) {
      res.result(http::status::internal_server_error);
      res.body() = "Internal server error";
      return;
    }
    
    // Close the file and read it as std::ifstream to actually get the content
    file.close();
    std::ifstream file_stream(path, std::ios::binary);
    std::string content((std::istreambuf_iterator<char>(file_stream)),
                        std::istreambuf_iterator<char>());
    res.result(http::status::ok);
    res.set(http::field::content_type, content_type);
    res.body() = content;
  }

  void handle_trickle(http::request<http::string_body>&& req,
                     http::response<http::string_body>& res) {
    try {
      // Parse worker ID
      auto worker_id = parse_worker_id(std::move(req));
      
      // Update last active time
      if(!worker_state_->update_activity(worker_id)) {
        res.result(http::status::bad_request);
        res.body() = "FAIL";
      } else {
        res.result(http::status::ok);
        res.body() = "OK";
      }
    } catch (const std::exception& e) {
      res.result(http::status::bad_request);
      res.body() = "FAIL";
    }
    res.prepare_payload();
  }
};

class DistributedSearchServer : public std::enable_shared_from_this<DistributedSearchServer> {
private:
  net::io_context& ioc_;
  tcp::acceptor acceptor_;
  
  // Work distribution system
  std::shared_ptr<WorkDistributionQueue> pending_work_;
  std::shared_ptr<WorkerStateManager> worker_state_;
  std::shared_ptr<WorkReturnQueue> pending_return_;
  
  // Admin interface
  std::shared_ptr<AdminBroadcaster> admin_broadcaster_;
  
  // Connection management
  std::atomic<size_t> active_connections_{0};
  const size_t max_connections_ = 10000;
  
  // Size limits
  const size_t max_work_unit_size_ = 320 * 1024 * 1024; // 320MB
  const size_t max_request_size_ = 640 * 1024 * 1024; // 640MB
  int returns_per_second = 500;
  
  // Timers
  net::steady_timer work_maintenance_timer_;
  net::steady_timer worker_cleanup_timer_;
  
  // Strands for thread-safe operations
  net::strand<net::io_context::executor_type> work_strand_;
  net::strand<net::io_context::executor_type> stats_strand_;

  // Loop index for calling the every256loops callback (todo refactor)
  std::atomic<int> maintain_work_queue_loop_index_{0};

public:
  DistributedSearchServer(net::io_context& ioc, tcp::endpoint endpoint)
    : ioc_(ioc)
    , acceptor_(ioc)
    , pending_work_(std::make_shared<WorkDistributionQueue>(MAX_PENDING_WORK, MAX_PENDING_WEIGHT))
    , worker_state_(std::make_shared<WorkerStateManager>())
    , pending_return_(std::make_shared<WorkReturnQueue>())
    , admin_broadcaster_(g_admin_broadcaster = std::make_shared<AdminBroadcaster>())
    , work_maintenance_timer_(ioc)
    , worker_cleanup_timer_(ioc)
    , work_strand_(ioc.get_executor())
    , stats_strand_(ioc.get_executor()) {
    
    acceptor_.open(endpoint.protocol());
    acceptor_.set_option(net::socket_base::reuse_address(true));
    acceptor_.bind(endpoint);
    acceptor_.listen(net::socket_base::max_listen_connections);
  }

  // Start accepting connections
  void run() {
    // accept();
    net::dispatch(
      acceptor_.get_executor(),
      beast::bind_front_handler(
        &DistributedSearchServer::accept,
        shared_from_this()));
    maintain_work_queue();
    cleanup_inactive_workers();
  }

private:
  void accept() {
    acceptor_.async_accept(
      net::make_strand(ioc_),
      beast::bind_front_handler(
        &DistributedSearchServer::on_accept,
        shared_from_this()));
  }

  void on_accept(beast::error_code ec, tcp::socket socket) {
    if (ec) {
      // Handle error
      fail(ec, "accept");
      return;
    }
    
    if (active_connections_.load() >= max_connections_) {
      // Reject new connection
      beast::error_code ignore_ec;
      socket.shutdown(tcp::socket::shutdown_both, ignore_ec);
      socket.close(ignore_ec);
      return;
    }
    
    active_connections_++;
    
    // Create and run session
    std::make_shared<HttpSession>(
      std::move(socket),
      pending_work_,
      worker_state_,
      pending_return_,
      std::function<void()>([self = shared_from_this()]() { self->active_connections_--; }),
      max_request_size_)->run();
    
    // Accept next connection
    accept();
  }

  void maintain_work_queue() {
    // Schedule periodic work queue maintenance on the work strand
    net::post(work_strand_,
      [self = shared_from_this()]() {
        // Fill pending work queue up to configured limits
        while (self->pending_work_->size() < MAX_PENDING_WORK &&
               self->pending_work_->total_weight() < MAX_PENDING_WEIGHT) {
          int workunitId, workunitWeight;
          std::string workunitData;
          
          if (!searchlogic::getWork(workunitId, workunitWeight, workunitData)) {
            break;
          }
          
          WorkUnit unit{workunitId, workunitWeight, std::move(workunitData)};
          if (!self->pending_work_->try_push(std::move(unit))) {
            // Queue full, stop filling
            searchlogic::relinquishWork(workunitId);
            break;
          }
        }

        // Process completed work units
        int cnt = 0;
        WorkResult result;
        while (self->pending_return_->pop(result)) {
          if (result.result.size()) {
            // WARN << "returning " << result.id << "\n";
            searchlogic::returnWork(result.id, result.result, result.contributor);
          } else {
            searchlogic::relinquishWork(result.id);
          }
          if (++cnt >= self->returns_per_second) {
            break;
          }
        }

        // Call every256loops callback if needed
        if (++self->maintain_work_queue_loop_index_ % 256 == 0) {
          searchlogic::every256Loops(
            self->worker_state_->get_worker_count(),
            self->pending_work_->size(),
            self->pending_return_->size());
        }
        
        // Schedule next maintenance
        self->work_maintenance_timer_.expires_after(
          std::chrono::seconds(1));
        self->work_maintenance_timer_.async_wait(
          [self](beast::error_code ec) {
            if (!ec) self->maintain_work_queue();
          });
      });
  }

  void cleanup_inactive_workers() {
    // Schedule periodic cleanup on a timer
    net::post(stats_strand_,
      [self = shared_from_this()]() {
        // Cleanup inactive workers and reclaim their work units
        auto reclaimed = self->worker_state_->cleanup_inactive(WORKER_TIMEOUT);
        
        for (int work_id : reclaimed) {
          searchlogic::relinquishWork(work_id);
        }
        
        // Schedule next cleanup
        self->worker_cleanup_timer_.expires_after(WORKER_PING_INTERVAL);
        self->worker_cleanup_timer_.async_wait(
          [self](beast::error_code ec) {
            if (!ec) self->cleanup_inactive_workers();
          });
      });
  }
};

int main(int argc, char* argv[]) {
  if (argc != 4) {
    std::cerr << "Usage: " << argv[0] << " <address> <port> <threads>\n";
    return EXIT_FAILURE;
  }
  
  auto address = net::ip::make_address(argv[1]);
  auto port = static_cast<unsigned short>(std::atoi(argv[2]));
  auto threads = std::max<int>(1, std::atoi(argv[3]));
  
  net::io_context ioc{threads};
  std::make_shared<DistributedSearchServer>(ioc, tcp::endpoint{address, port})->run();
  
  net::signal_set signals(ioc, SIGINT, SIGTERM);
  signals.async_wait([&ioc](const beast::error_code&, int) { ioc.stop(); });

  std::vector<std::thread> v;
  v.reserve(threads - 1);
  for (auto i = threads - 1; i > 0; --i) {
    v.emplace_back([&ioc] { ioc.run(); });
  }
  ioc.run();

  // io_context::run() will return if SIGINT or SIGTERM is received
  
  for (auto& t : v) {
    t.join();
  }
  
  return EXIT_SUCCESS;
}
