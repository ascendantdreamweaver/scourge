#pragma once

// #include <boost/lockfree/queue.hpp>
// #include <boost/lockfree/spsc_queue.hpp>
#include <atomic>
#include <chrono>
#include <mutex>
#include <shared_mutex>
#include <unordered_map>
#include <set>

#include "cqueue/concurrentqueue.h"

// Work unit structure
struct WorkUnit {
  int id;
  int weight;
  std::string data;
  
  // For priority queue ordering
  bool operator<(const WorkUnit& other) const {
    return weight < other.weight;
  }
};

// Work result structure
struct WorkResult {
  int id;
  std::string contributor;
  std::string result;
};

// Thread-safe work distribution queue
class WorkDistributionQueue {
private:
  moodycamel::ConcurrentQueue<WorkUnit> queue_;
  // boost::lockfree::queue<WorkUnit> queue_;
  std::atomic<size_t> size_{0};
  std::atomic<size_t> total_weight_{0};
  
  // Configuration
  const size_t max_size_;
  const size_t max_weight_;
  
public:
  WorkDistributionQueue(size_t max_size, size_t max_weight)
    : queue_(max_size), max_size_(max_size), max_weight_(max_weight) {}
    
  // Try to add work unit
  bool try_push(WorkUnit&& unit) {
    if (size_.load() >= max_size_ || 
      total_weight_.load() + unit.weight > max_weight_) {
      return false;
    }
    
    int unit_weight = unit.weight;
    if (queue_.enqueue(std::move(unit))) {
      size_++;
      total_weight_ += unit_weight;
      return true;
    }
    return false;
  }
  
  // Try to get work units up to max_weight
  std::vector<WorkUnit> try_pop(int max_weight) {
    std::vector<WorkUnit> result;
    WorkUnit unit;
    int current_weight = 0;
    
    while (queue_.try_dequeue(unit)) {
      if (current_weight == 0 || // give at least one work unit
          current_weight + unit.weight <= max_weight) {
        current_weight += unit.weight;
        size_--;
        total_weight_ -= unit.weight;
        result.push_back(std::move(unit));
        if (current_weight >= max_weight) {
          break;
        }
      } else {
        // Put it back if it doesn't fit
        queue_.enqueue(std::move(unit));
        break;
      }
    }
    
    return result;
  }
  
  size_t size() const { return size_.load(); }
  size_t total_weight() const { return total_weight_.load(); }
};

class WorkReturnQueue {
private:
  moodycamel::ConcurrentQueue<WorkResult> queue_;
  // boost::lockfree::queue<WorkResult> queue_;
  
public:
  void push(WorkResult&& result) {
    queue_.enqueue(std::move(result));
  }
  
  bool pop(WorkResult& result) {
    return queue_.try_dequeue(result);
  }

  size_t size() const { return queue_.size_approx(); }
};

// Worker connection state
struct WorkerState {
  std::chrono::steady_clock::time_point creation_time;
  std::chrono::steady_clock::time_point last_active;
  std::set<int> active_workunits;
  uint64_t latency;
};

// Thread-safe worker state manager
class WorkerStateManager {
private:
  mutable std::shared_mutex mutex_;
  std::unordered_map<uint64_t, WorkerState> workers_;
  std::atomic<uint64_t> next_worker_id_{0};
  
public:
  // Get unique worker ID
  uint64_t get_unique_worker_id() {
    int next_id = ++next_worker_id_;
    std::unique_lock lock(mutex_);
    workers_[next_id].creation_time = std::chrono::steady_clock::now();
    workers_[next_id].last_active = workers_[next_id].creation_time;
    return next_id;
  }

  // Update worker activity
  bool update_activity(uint64_t worker_id) {
    std::unique_lock lock(mutex_);
    auto it = workers_.find(worker_id);
    if (it != workers_.end()) {
      it->second.last_active = std::chrono::steady_clock::now();
      return true;
    } else {
      return false;
    }
  }
  
  // Assign work to worker
  void assign_work(uint64_t worker_id, int work_id) {
    std::unique_lock lock(mutex_);
    auto& worker = workers_[worker_id];
    worker.active_workunits.insert(work_id);
  }
  
  // Complete work from worker
  void complete_work(uint64_t worker_id, int work_id) {
    std::unique_lock lock(mutex_);
    auto& worker = workers_[worker_id];
    worker.active_workunits.erase(work_id);
    worker.last_active = std::chrono::steady_clock::now();
  }

  // Elevate worker to permanent
  void elevate_worker(uint64_t worker_id) {
    std::unique_lock lock(mutex_);
    auto& worker = workers_[worker_id];
    worker.latency = 16777216;
  }
  
  // Get worker stats (thread-safe read)
  WorkerState get_stats(uint64_t worker_id) const {
    std::shared_lock lock(mutex_);
    if (auto it = workers_.find(worker_id); it != workers_.end()) {
      return it->second;
    }
    return WorkerState{};
  }

  // Get worker count (thread-safe read)
  size_t get_worker_count() const {
    std::shared_lock lock(mutex_);
    return workers_.size();
  }

  // Vector of uptimes in ms
  std::vector<uint64_t> get_uptimes() const {
    std::shared_lock lock(mutex_);
    std::vector<uint64_t> uptimes;
    for (const auto& [_, worker] : workers_) {
      uptimes.push_back(std::chrono::duration_cast<std::chrono::milliseconds>(
        worker.last_active - worker.creation_time).count());
    }
    return uptimes;
  }
  
  // Cleanup inactive workers. Returns deleted work units
  std::vector<int> cleanup_inactive(std::chrono::milliseconds timeout) {
    std::unique_lock lock(mutex_);
    std::vector<int> reclaimed;
    auto now = std::chrono::steady_clock::now();
    
    for (auto it = workers_.begin(); it != workers_.end(); ) {
      if (now - it->second.last_active > timeout && 
        it->second.latency != 16777216) {
        reclaimed.insert(reclaimed.end(), 
          it->second.active_workunits.begin(), 
          it->second.active_workunits.end());
        it = workers_.erase(it);
      } else {
        ++it;
      }
    }
    
    return reclaimed;
  }
};
