import time
import sys
import os

def spawncondor(infiles):
  fn = f"job-{int(time.time())}.submit"
  f = open(fn, "w")
  ostdout = sys.stdout
  sys.stdout = f
  print(f"""executable = vriska_core
request_cpus = 1
request_memory = 1GB
request_disk = 1GB
on_exit_hold = (ExitBySignal == True) || (ExitCode != 0)
periodic_release = NumJobStarts < 10
error = err
output = out
log = log
  """)
  print()
  cnt = 0
  for i in infiles:
    print(f"transfer_input_files = infiles/{i}")
    print(f"arguments = {i} tmp{i}.out")
    print(f'transfer_output_files = tmp{i}.out')
    print(f'transfer_output_remaps = "tmp{i}.out=outfiles/{i}.out"')
    print("queue 1")
  f.close()
  sys.stdout = ostdout
  #os.system(f"condor_submit {fn}")
  print(f"QU3U3D {len(infiles)} JOBS TO {fn}")
