import asyncio
import aiohttp
import sys
import io
import os
import backoff

import condor

def write_int(x: int, f: io.BufferedWriter):
  """
  Writes an integer x to a binary stream f using 7-bit encoding.
  """
  while x >= 128:
    y = (x & 127) | 128
    f.write(bytes([y]))
    x >>= 7
  f.write(bytes([x]))

def write_string(data: bytes, f: io.BufferedWriter):
  """
  Writes a binary string (raw bytes) data to a binary stream f, prefixed with its length in 7-bit encoded format.
  """
  write_int(len(data), f)
  f.write(data)

def read_int(f: io.BufferedReader) -> int:
  """
  Reads an integer from a binary stream f using 7-bit encoding.
  """
  x = 0
  s = 0
  while True:
    y = ord(f.read(1))
    x |= (y & 127) << s
    if y & 128 == 0:
      break
    s += 7
    if s > 64:  # Prevent reading excessively large integers
      raise ValueError("Integer size too large.")
  return x

def read_string(f: io.BufferedReader, maxlen: int = 10_000_000) -> bytes:
  """
  Reads a binary string (raw bytes) from a binary stream f, prefixed by its length in 7-bit encoded format.
  """
  assert isinstance(f, io.BufferedReader)
  length = read_int(f)
  if length > maxlen:
    raise ValueError("String length exceeds maxlen.")
  return f.read(length)  # Read raw bytes

async def get_unique_id(session: aiohttp.ClientSession, host: str):
  async with session.get(f"{host}/getuniqueid") as response:
    # Check if the response is successful
    if response.status != 200:
      raise Exception(f"Failed to get unique ID: {response.status}")
    # convert string to int
    return int(await response.text())

@backoff.on_exception(backoff.constant,aiohttp.ClientError,max_tries=20)
async def keepaliveloop(session: aiohttp.ClientSession, host: str, unique_id: int):
  loop = f"{unique_id} 1 0".encode()
  while True:
    print("ping", unique_id)
    async with session.post(f"{host}/getwork", data=loop) as response:
      # print("e")
      if response.status != 200:
        raise Exception(f"Failed to keep alive: {response.status}")
      await response.read()
    await asyncio.sleep(10)

@backoff.on_exception(backoff.expo,aiohttp.ClientError,max_tries=20)
async def getwork(session: aiohttp.ClientSession, host: str, unique_id: int, amnt: int, targ: str, sem: asyncio.Semaphore):
  async with sem:
    loop = f"{unique_id} {amnt}".encode()
    await asyncio.sleep(1)
    async with session.post(f"{host}/getwork2", data=loop) as response:
      if response.status != 200:
        raise Exception(f"Failed to get work: {response.status}")
      resp = await response.read()
      print(len(resp))
      buf = io.BytesIO(resp)
      cnt = read_int(buf)
      print("got", cnt)
      with open(targ, "wb") as f:
        await asyncio.to_thread(f.write, resp)
      return (cnt, targ)

def inspect_returned(s: str):
  with open(s, "rb") as f:
    cnt = read_int(f)
    works = []
    for i in range(cnt):
      wuid = read_int(f)
      dat = read_string(f)
      works.append((wuid, dat))
    if read_string(f) != b"done":
      return []
    return works

@backoff.on_exception(backoff.expo,aiohttp.ClientError,max_tries=20)
async def returnwork(session: aiohttp.ClientSession, host: str, unique_id: int, s: str, sem: asyncio.Semaphore):
  async with sem:
    works = await asyncio.to_thread(inspect_returned, f"outfiles/{s}")
    if works:
      buf = io.BytesIO()
      write_int(len(works), buf)
      write_int(unique_id, buf)
      for wuid, dat in works:
        write_int(wuid, buf)
        write_int(1, buf)
        write_string(b'OSG', buf)
        write_string(dat, buf)
      async with session.post(f"{host}/returnwork", data=buf.getvalue()) as response:
        if response.status != 200:
          raise Exception(f"Failed to return work: {response.status}")
        resp = await response.text()
        assert resp.strip() == "OK"
      os.remove(f"outfiles/{s}")
      os.remove(f"infiles/{s[:-4]}")

async def main_loop(unique_id = -1):
  host = f"http://{sys.argv[1]}" if len(sys.argv) > 1 else "http://localhost:8000"
  connector = aiohttp.TCPConnector(force_close=True)
  sem = asyncio.Semaphore(10)
  async with aiohttp.ClientSession(connector=connector) as session:
    if unique_id == -1:
      unique_id = await get_unique_id(session, host)
    # spawn keep alive loop
    keepalive = asyncio.create_task(keepaliveloop(session, host, unique_id))
    # with tempfile.NamedTemporaryFile() as f:
    cnt = 10
    v = 0
    while 1:
      files = await asyncio.to_thread(os.listdir, "outfiles")
      await asyncio.gather(*[returnwork(session, host, unique_id, s, sem) for s in files])
      await asyncio.sleep(10)
      cnt = 500
      res = []
      for got, fn in await asyncio.gather(*[getwork(session, host, unique_id, 5000, f"infiles/{v}-{i}.txt", sem) for i in range(cnt)]):
        if got:
          res += [fn]
        else:
          os.remove(fn)
      if res:
        assert all(i.startswith("infiles/") for i in res)
        res = [i[8:] for i in res]
        await asyncio.to_thread(condor.spawncondor, res)
      await asyncio.sleep(60)
      v += 1

s = int(input("elevated id: "))
asyncio.run(main_loop(s))