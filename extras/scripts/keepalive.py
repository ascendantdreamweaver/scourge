import asyncio
import aiohttp
import sys
import io
import subprocess
import os

async def keepaliveloop(session: aiohttp.ClientSession, host: str, unique_id: int):
  loop = f"{unique_id} 1 0".encode()
  while True:
    print("ping", unique_id)
    async with session.post(f"{host}/getwork", data=loop) as response:
      # print("e")
      if response.status != 200:
        raise Exception(f"Failed to keep alive: {response.status}")
      await response.read()
    await asyncio.sleep(10)

async def main_loop(unique_id):
  host = f"http://{sys.argv[1]}" if len(sys.argv) > 1 else "http://localhost:8000"
  connector = aiohttp.TCPConnector(force_close=True)
  async with aiohttp.ClientSession(connector=connector) as session:
    # spawn keep alive loop
    keepalive = asyncio.create_task(keepaliveloop(session, host, unique_id))
    await keepalive

asyncio.run(main_loop(2))