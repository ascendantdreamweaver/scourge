#include "mongoose.h"

#include <bits/stdc++.h>

#include "cqueue/bcq.h"

const int worker_timeout_duration = 100000; // ms
const int worker_ping_rate = 50000; // ms
const int maxProcessedWUperSec = 1000000;
const int maintainCachedWUs = 3000; // workunits

#include "search.h"
#include "cache.h"

Cache pendingOutboundCache("outbound.dbm");
moodycamel::BlockingConcurrentQueue<std::pair<int,int>> pendingOutbound; // pair (id, weight)

Cache pendingInboundCache("inbound.dbm");
struct pendingInboundMessage {
  // state: c - completed, u - unfinished, s - just sent
  // cid is *connection* id
  int id; unsigned long cid; char state; std::string contributor;
};
moodycamel::BlockingConcurrentQueue<pendingInboundMessage> pendingInbound;

std::string _mg_str_to_stdstring(mg_str x) {
  return std::string(x.buf, x.buf + x.len);
}

struct adminhandlerMessage {
  int flag = 0; // 1 - broadcast, 2 - halt all connections, 4 - connection closed,
  unsigned long conn_id;  // Parent connection ID
  std::string message;
};

struct wakeupCall {
  int flag = 0; // 1 - WS, 2 - WS close conn, 16 - handled get request, 17 - handled return request
  std::string message;
};

std::map<unsigned long, mg_mgr*> adminConnections;
std::mutex adminConnections_mutex;
moodycamel::BlockingConcurrentQueue<adminhandlerMessage> adminConsoleHandler_queue;
int adminConsoleHandler_running = 0;
void woker(mg_mgr* const& mgr, const unsigned long conn, const wakeupCall& s) {
  wakeupCall* resp = new wakeupCall {s};
  mg_wakeup(mgr, conn, &resp, sizeof(resp));
}

void adminConsoleBroadcast(const std::string& s) {
  adminConsoleHandler_queue.enqueue({1, 0, s});
}

struct workerInfo {
  uint64_t contime, uptime, latency;
  uint64_t lastmsg;
  std::set<int> attachedWorkunits;
};
std::map<unsigned long, workerInfo> workerConnections;
std::mutex workerConnections_mutex;

void workunitHandler() {
  // every T seconds clear out B2A and regenerate A2B node
  pendingInboundMessage nx;
  int idx = 0;
  while(1) {
    std::this_thread::sleep_for(1 * std::chrono::seconds(1));
    // process pendingInbound but process at most 1000 to ensure that pendingOutbound can be refreshed
    std::vector<std::pair<int,int>> addPendingOutbound;
    std::vector<pendingInboundMessage> to_process(maxProcessedWUperSec);
    to_process.resize(pendingInbound.try_dequeue_bulk(to_process.begin(), maxProcessedWUperSec));
    { const std::lock_guard<std::mutex> lock2(workerConnections_mutex);
      for(auto& x:to_process) {
        if(x.state == 'c') {
          std::string workunitData = pendingInboundCache.get(x.id);
          pendingInboundCache.erase(x.id);
          searchlogic::returnWork(x.id, workunitData, x.contributor);
        } else if(x.state == 'u') {
          searchlogic::relinquishWork(x.id);
        }
        if(x.state == 'c' || x.state == 'u') {
          if(workerConnections.find(x.cid) != workerConnections.end())
            if(workerConnections[x.cid].attachedWorkunits.contains(x.id))
              workerConnections[x.cid].attachedWorkunits.erase(x.id);
        } else {
          // note that only workerhandler is allowed to delete from workerconnections
          // so if we somehow sent to a deleted connection, recycle it immediately
          if(workerConnections.find(x.cid) != workerConnections.end())
            workerConnections[x.cid].attachedWorkunits.insert(x.id);
          else
            searchlogic::relinquishWork(x.id);
        }
      }
    }
    int szapprox = pendingOutbound.size_approx();
    while (szapprox + addPendingOutbound.size() < maintainCachedWUs) {
      int workunitId, workunitWeight; std::string workunitData;
      if(!searchlogic::getWork(workunitId, workunitWeight, workunitData)) break;
      pendingOutboundCache.set(workunitId, workunitData);
      addPendingOutbound.push_back({workunitId, workunitWeight});
    }
    pendingOutbound.enqueue_bulk(addPendingOutbound.begin(), addPendingOutbound.size());
    if((++idx) % 256 == 0) {
      const std::lock_guard<std::mutex> lock2(workerConnections_mutex);
      searchlogic::every256Loops(workerConnections.size(), pendingOutbound.size_approx(), pendingInbound.size_approx());
    }
  }
}

std::thread WUhandlerthread(workunitHandler);

// note adminConsoleHandler can block thread during runs
void adminConsoleHandler() {
  assert(adminConsoleHandler_running == 1);
  adminhandlerMessage nx;
  while (adminConsoleHandler_queue.wait_dequeue(nx), nx.flag != 2) {
    MG_INFO(("handling %d - %s", nx.conn_id, nx.message.c_str()));
    if(nx.flag == 1) {
      const std::lock_guard<std::mutex> lock(adminConnections_mutex);
      for(auto& [conn, mgr] : adminConnections)
        woker(mgr, conn, {1, nx.message});
      continue;
    }
    if(nx.flag == 4) {
      const std::lock_guard<std::mutex> lock(adminConnections_mutex);
      auto it =  adminConnections.find(nx.conn_id);
      if(it != adminConnections.end())
        adminConnections.erase(it);
      continue;
    }
    mg_mgr* mgr;
    { const std::lock_guard<std::mutex> lock(adminConnections_mutex);
      auto it =  adminConnections.find(nx.conn_id);
      if(it == adminConnections.end())
        continue; // bizarre
      mgr = adminConnections[nx.conn_id];
    }
    if(nx.flag == 3) {
      // New connection initialization
      searchlogic::adminConsoleInitConn([&mgr, &nx](const wakeupCall& s) {
        woker(mgr, nx.conn_id, s);
      });
      continue;
    }
    std::istringstream s(nx.message);
    std::string com; s>>com;
    if(com == "help") {
      std::ostringstream f;
      f << "commands: remoteclose connstats elevate ";
      f << searchlogic::adminConsoleCommands;
      woker(mgr, nx.conn_id, {1, f.str()});
    } else if(com == "remoteclose") {
      woker(mgr, nx.conn_id, {3, "bye"});
    } else if(com == "connstats") {
      std::ostringstream f;
      std::vector<uint64_t> uptimes;
      { const std::lock_guard<std::mutex> lock(workerConnections_mutex);
        for(auto& [conn, m]:workerConnections) uptimes.push_back(m.uptime);
      }
      f << uptimes.size() << " connections<br/>uptimes:";
      f<<std::fixed<<std::setprecision(1);
      for(auto i: uptimes) f<<" "<<i/60000.0<<"m";
      woker(mgr, nx.conn_id, {1, f.str()});
    } else if (com == "elevate") {
      int id; s>>id;
      { const std::lock_guard<std::mutex> lock(workerConnections_mutex);
        workerConnections[id].latency = 16777216;
      }
      std::ostringstream f;
      f << "elevated connection "<<id<<" to permanent\n";
      woker(mgr, nx.conn_id, {1, f.str()});
    } else {
      searchlogic::adminConsoleCommandHandler(com, s, [&mgr, &nx](const wakeupCall& s) {
        woker(mgr, nx.conn_id, s);
      });
    }
  }
  // halt all connections
  { const std::lock_guard<std::mutex> lock(adminConnections_mutex);
    // TODO. lmao.
    // hmm does this trigger nx flag 4 ...?
    adminConnections.clear();
  }
  adminConsoleHandler_running = false;
}

struct workerhandlerMessage {
  int flag = 0; // 1 - loop over everyone
  unsigned long conn;
  unsigned long mil;
};

moodycamel::BlockingConcurrentQueue<workerhandlerMessage> workerHandler_queue;
void postWorkerDisconnect(unsigned long conn) {
  for(int i:workerConnections[conn].attachedWorkunits) // this is done with a lock on it anyways
    pendingInbound.enqueue({i, conn, 'u', ""});
}
void workerHandler() {
  std::this_thread::sleep_for(std::chrono::milliseconds(20));
  MG_INFO(("Worker handler started running"));
  workerhandlerMessage nx;
  // Every [worker_ping_rate], go through all of the worker connections; kick the ones that haven't sent anything in [worker_timeout_duration]
  while (workerHandler_queue.wait_dequeue(nx), nx.flag != 2) {
    if(nx.flag == 0) {
      const std::lock_guard<std::mutex> lock(workerConnections_mutex);
      auto& pp = workerConnections[nx.conn];
      if(!pp.contime) pp.contime = nx.mil;
      pp.lastmsg = nx.mil;
      pp.uptime = nx.mil - pp.contime;
    }
    if(nx.flag == 1) {
      const std::lock_guard<std::mutex> lock(workerConnections_mutex);
      std::vector<unsigned long> toKick;
      for(auto& [conn, stat] : workerConnections) 
        if(stat.contime && stat.lastmsg + worker_timeout_duration < nx.mil && stat.latency != 16777216) {
          std::cerr << "kicked " << conn << '\n';
          toKick.push_back(conn);
        }
      for(auto conn : toKick) {
        auto it = workerConnections.find(conn);
        if(it != workerConnections.end()) {
          postWorkerDisconnect(conn);
          workerConnections.erase(it);
        }
      }
      continue;
    }
  }
  // halt all connections
  { const std::lock_guard<std::mutex> lock(workerConnections_mutex);
    for(auto & [conn, _] : workerConnections)
      postWorkerDisconnect(conn);
    workerConnections.clear();
  }
}

std::thread workerhandlerthread(workerHandler);

struct getWorkRequestMessage {
  int hrc; unsigned long wsid;
  unsigned long conn; 
  mg_mgr* mgr;
};

moodycamel::BlockingConcurrentQueue<getWorkRequestMessage> getWorkRequestHandler_queue;

void getWorkRequestHandler() {
  std::this_thread::sleep_for(std::chrono::milliseconds(30));
  MG_INFO(("Getwork request handler started running"));
  getWorkRequestMessage nx;
  while (getWorkRequestHandler_queue.wait_dequeue(nx), true) {
    int hrc = nx.hrc; unsigned long wsid = nx.wsid;
    std::ostringstream res(std::ios::out | std::ios::binary);
    std::vector<std::pair<int,int>> nodes;
    int total = 0;
    std::pair<int,int> wu;
    while(pendingOutbound.try_dequeue(wu)) {
      int s = wu.second;
      if(s >= hrc) {
        pendingOutbound.enqueue_bulk(nodes.begin(), nodes.size());
        nodes = {wu};
        break;
      } else {
        total += s;
        nodes.push_back(wu);
        if(total >= hrc) break;
      }
    }
    writeInt(nodes.size(), res);
    for(auto& [pom, wt]:nodes) {
      pendingInbound.enqueue({pom, wsid, 's', ""});
      writeInt(pom, res);
      writeString(pendingOutboundCache.get(pom), res);
      pendingOutboundCache.erase(pom);
    }
    woker(nx.mgr, nx.conn, {16, res.str()});
  }
}

std::thread gethandlerthread(getWorkRequestHandler);

struct returnWorkRequestMessage {
  std::string dat;
  unsigned long conn; 
  mg_mgr* mgr;
};

moodycamel::BlockingConcurrentQueue<returnWorkRequestMessage> returnWorkRequestHandler_queue;

void returnWorkRequestHandler() {
  std::this_thread::sleep_for(std::chrono::milliseconds(40));
  MG_INFO(("Returnwork request handler started running"));
  returnWorkRequestMessage nx;
  while (returnWorkRequestHandler_queue.wait_dequeue(nx), true) {
    std::istringstream co(nx.dat, std::ios::in | std::ios::binary);
    int amnt;
    readInt(amnt, co);
    unsigned long wsid;
    readInt(wsid, co);
    for(int i=0; i<amnt; i++) {
      std::string cid;
      int id=0, status=0;
      readInt(id, co);
      readInt(status, co);
      readString(cid, co);
      if(status == 0)
        pendingInbound.enqueue({id, wsid, 'u', ""}); // tofix
      else {
        std::string v;
        readString(v, co);
        if(co.fail()) goto fail;
        pendingInboundCache.set(id, v);
        pendingInbound.enqueue({id, wsid, 'c', cid}); // tofix
      }
    }
    workerHandler_queue.enqueue({0, wsid, mg_millis()});
    if(co.fail()) goto fail;
    woker(nx.mgr, nx.conn, {17, "OK"});
    continue;
    fail:
    std::cerr << "returnwork failed" << std::endl;
    woker(nx.mgr, nx.conn, {17, "FAIL"});
  }
}

std::thread returnhandlerthread(returnWorkRequestHandler);


mg_str ca_cert, server_cert, server_key;

// HTTP request callback
void fn(mg_connection* c, int ev, void* ev_data) {
  if (ev == MG_EV_HTTP_MSG) {
    mg_http_message* hm = (mg_http_message*) ev_data;
    /* admin end */
    if(mg_match(hm->uri, mg_str("/"), NULL)) {
      mg_http_serve_opts opts = {.root_dir=".", .ssi_pattern=NULL, .extra_headers=NULL, .mime_types="html=text/html", .page404=NULL, .fs=NULL};
      mg_http_serve_file(c, hm, "index.html", &opts);
    } else if(mg_match(hm->uri, mg_str("/admin"), NULL)) {
      mg_http_serve_opts opts = {.root_dir=".", .ssi_pattern=NULL, .extra_headers=NULL, .mime_types="html=text/html", .page404=NULL, .fs=NULL};
      mg_http_serve_file(c, hm, "admin.html", &opts);
    } else if(mg_match(hm->uri, mg_str("/admin-websocket"), NULL)) {
      mg_ws_upgrade(c, hm, NULL);
      c->data[0] = 'W'; // websocket
      c->data[1] = 'A'; // admin
      { const std::lock_guard<std::mutex> lock(adminConnections_mutex);
        adminConnections[c->id] = c->mgr;
      }
      if(adminConsoleHandler_running == 0) {
        adminConsoleHandler_running++;
        std::thread t(adminConsoleHandler); t.detach();
      }
      // Queue connection initialization to adminConsoleHandler
      adminConsoleHandler_queue.enqueue({3, c->id, ""});
    } else if(mg_match(hm->uri, mg_str("/getuniqueid"), NULL)) {
      static unsigned long id = 0;
      id++;
      std::ostringstream res; res << id;
      mg_http_reply(c, 200, "Content-Type: text/raw\n", "%s", res.str().c_str());
    } else if(mg_match(hm->uri, mg_str("/trickle"), NULL)) { // Alert the server the worker is still here
      // POST request, should contain only websocket id
      std::istringstream co(_mg_str_to_stdstring(hm->body));
      unsigned long wsid=0;
      co >> wsid;
      if(co.fail())
        mg_http_reply(c, 500, "Content-Type: text/raw\n", "FAIL");
      else {
        workerHandler_queue.enqueue({0, wsid, mg_millis()});
        mg_http_reply(c, 200, "Content-Type: text/raw\n", "OK");
      }
    } else if(mg_match(hm->uri, mg_str("/getwork2"), NULL)) {
      // POST request, should contain two parameters: websocket id & max sum(weight)
      // returns amount * [workunit identifier]
      std::istringstream co(_mg_str_to_stdstring(hm->body));
      unsigned long wsid=0;
      co >> wsid;
      int hrc=0; co >> hrc;
      if(!(0 <= hrc && hrc <= 100000) || co.fail()) {
        std::cerr << "getwork failed: " << _mg_str_to_stdstring(hm->body) << std::endl;
        mg_http_reply(c, 200, "Content-Type: text/raw\n", "-1");
        return;
      }
      workerHandler_queue.enqueue({0, wsid, mg_millis()});
      getWorkRequestHandler_queue.enqueue({hrc, wsid, c->id, c->mgr});
    } else if(mg_match(hm->uri, mg_str("/returnwork"), NULL)) {
      // POST request, should contain amount, websocket connection id
      // then amount times the following: workunit id, status - 0 or 1
      // if status is 1, further contain contributor id and all children
      returnWorkRequestHandler_queue.enqueue({_mg_str_to_stdstring(hm->body), c->id, c->mgr});
    } 
    /* ?? */
    else {
      MG_INFO(("unknown endpoint %.*s", hm->uri.len, hm->uri.buf));
      mg_http_reply(c, 404, "Content-Type: text/raw\n", "what\n");
    }
  } else if(ev == MG_EV_WS_CTL) {
    mg_ws_message* wm = (mg_ws_message*) ev_data;
    uint32_t s = (wm->flags) & 0xF0;
    MG_INFO(("\033[1;1;31mwebsocket control triggered, %.*s, %x \033[0m", wm->data.len, wm->data.buf, s));
    if(s == 0x80) {
      if(c->data[1] == 'A')
        adminConsoleHandler_queue.enqueue({4, c->id, ""});
    }
  } else if(ev == MG_EV_WS_MSG) {
    mg_ws_message* wm = (mg_ws_message*) ev_data;
    if(c->data[1] == 'A') {
      adminConsoleHandler_queue.enqueue({0, c->id, _mg_str_to_stdstring(wm->data)});
    }
  } else if (ev == MG_EV_WAKEUP) {
    // struct mg_str *data = (struct mg_str *) ev_data;
    wakeupCall* data = * (wakeupCall**) ((mg_str*) ev_data)->buf;
    if(data->flag < 16) {
      if(data->flag & 1) {
        auto sv = mg_str(data->message.c_str());
        if(sv.len)
          mg_ws_send(c, sv.buf, sv.len, WEBSOCKET_OP_TEXT);
        if(data->flag & 2) {
          mg_ws_send(c, NULL, 0, WEBSOCKET_OP_CLOSE);
        }
      } else {
        mg_http_reply(c, 200, "", "Result: %s\n", data->message.c_str());
      } 
    } else if(data->flag == 16) {
      // getwork finished
      mg_printf(c,
              "HTTP/1.1 200 OK\nContent-Type: octet-stream\nContent-Length: %d\n\n",
              (int)data->message.size());
      mg_send(c, data->message.data(), (int)data->message.size()); 
    } else if(data->flag == 17) {
      // returnwork finished
      if(data->message == "OK")
        mg_http_reply(c, 200, "Content-Type: text/raw\n", "OK");
      else
        mg_http_reply(c, 200, "Content-Type: text/raw\n", "FAIL");
    }
    delete data;
  } else if (ev == MG_EV_POLL) {
    static uint64_t lastping = 0, current = 0;
    if((current = mg_millis()) > lastping + worker_ping_rate) {
      lastping = current;
      workerHandler_queue.enqueue({1, 0, lastping});
    }
  }
}

int main(int argc, const char* argv[]) {
  mg_mgr mgr;
  mg_mgr_init(&mgr);        // Initialise event manager
  mg_log_set(MG_LL_INFO); 
  std::string hostname = "http://localhost:8000";
  if(argc != 1) hostname = argv[1];
  mg_http_listen(&mgr, hostname.c_str(), fn, NULL);  // Create listener
  std::cerr << "Listening on " << hostname << std::endl;
  mg_wakeup_init(&mgr);  // Initialise wakeup socket pair
  for (;;) {             // Event loop
    mg_mgr_poll(&mgr, 1000);
  }
  mg_mgr_free(&mgr);
  return 0;
}
