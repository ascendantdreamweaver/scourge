#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <sstream>
#include <vector>
#include <thread>
#include <vector>
#include <sstream>

#include "debug.h"
#include "globals.h"
#include "logic.h"
#include "searchtree.h"

// Ensure all generated files have between SIZE_LIMIT and SIZE_LIMIT*2 halfrows, or only one workunit > SIZE_LIMIT
const int SIZE_LIMIT = 5000; 

int main(int argc, char* argv[]) {
  if (argc != 3) {
    std::cerr << "usage: " << argv[0] << " [inputfile] [targpfx]\n";
    return 1;
  }
  std::ifstream f1(argv[1], std::ios::in | std::ios::binary);
  std::map<int, int> wuSizes;
  std::map<int, int> triggers; // 0: continue, 1: start new, 2: spawn new, do not discard last 1
  std::vector<std::vector<int>> wuAssignment;
  std::vector<int> order;
  while(1) {
    int cnt; readInt(cnt, f1);
    WARN << cnt << '\n';
    if(cnt == 0) break;
    for(int i=0; i<cnt; i++) {
      int pid; std::string v;
      readInt(pid, f1); readString(v, f1);
      std::stringstream fn(v, std::ios::in | std::ios::binary);
      searchTree* T = loadf(fn, "loadWorkunit");
      int ts = 0;
      for (int i = 0; i < T->treeSize; i++) {
        if (T->a[i].n[0] == 0 || T->a[i].n[1] == 0)
          T->a[i].tags &= ~TAG_QUEUED;
        if (T->a[i].tags & TAG_QUEUED)
          ts += T->a[i].n[0] + T->a[i].n[1];
      }
      WARN << pid << "(" << ts << ") ";
      if(ts >= SIZE_LIMIT) wuAssignment.push_back({pid}), triggers[pid] = -1;
      else wuSizes[pid] = ts, order.push_back(pid);
      delete T;
    }
    WARN << '\n';
  }
  int curSz = SIZE_LIMIT;
  for(auto pid:order) {
    int ts = wuSizes[pid];
    if(curSz >= SIZE_LIMIT) wuAssignment.push_back({pid}), curSz = ts, triggers[pid] = 1;
    else {
      wuAssignment.back().push_back({pid}), curSz += ts, triggers[pid] = 0;
      triggers[wuAssignment.back()[0]] = wuAssignment.back().size();
    }
  }
  f1.clear();
  f1.seekg(0);
  std::ofstream cur;
  std::ofstream list(std::string(argv[2])+"list.txt");
  int id = 0;
  while(1) {
    int cnt; readInt(cnt, f1);
    if(cnt == 0) break;
    for(int i=0; i<cnt; i++) {
      int pid; std::string v;
      readInt(pid, f1); readString(v, f1);
      if(triggers[pid] == 0) {
        writeInt(pid, cur);
        writeString(v, cur);
      } else if(triggers[pid] == -1) {
        std::string fn = argv[2] + std::to_string(id++) + ".txt";
        std::ofstream alt(fn, std::ios::out | std::ios::binary);
        writeInt(1, alt);
        writeInt(pid, alt);
        writeString(v, alt);
        alt.close();
        list << fn << '\n';
      } else {
        if(cur.is_open()) cur.close();
        std::string fn = argv[2] + std::to_string(id++) + ".txt";
        list << fn << '\n';
        cur.open(fn, std::ios::out | std::ios::binary);
        writeInt(triggers[pid], cur);
        writeInt(pid, cur);
        writeString(v, cur);
      }
    }
  }

  if(cur.is_open()) cur.close();
  return 0;
}
