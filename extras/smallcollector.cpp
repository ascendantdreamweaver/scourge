#include "cadical/src/cadical.hpp"

#include "cqueue/bcq.h"

#include "mongoose.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <sstream>
#include <vector>
#include <thread>
#include <vector>
#include <sstream>

using std::max;
using std::min;

#include "debug.h"
#include "globals.h"
#include "logic.h"
#include "satlogic.h"
#include "searchtree.h"

std::atomic_bool done = false;
long long cid = -1;
mg_connection* hostConnection;

// std::string s_url = "https://";
std::string s_url = "http://";
const uint64_t s_timeout_ms = 100000;  // Connect timeout in milliseconds

std::string _mg_str_to_stdstring(mg_str x) {
  return std::string(x.buf, x.buf + x.len);
}

struct wakeupCall {
  int flag = 0; // 0 - send getinfo, 1 - send getwork, 2 - send returnwork
  std::string message;
};
void woker(mg_mgr* const& mgr, const unsigned long conn, const wakeupCall& s) {
  wakeupCall* resp = new wakeupCall {s};
  mg_wakeup(mgr, conn, &resp, sizeof(resp));
}

wakeupCall t_response;

void handler_httpconn(mg_connection* c, int ev, void* ev_data) {
  // ripped from https://github.com/cesanta/mongoose/blob/master/tutorials/http/http-client/main.c
  if (ev == MG_EV_OPEN) {
    // std::cerr << "connection opened\n";
    *(uint64_t *) c->data = mg_millis();
  } else if (ev == MG_EV_POLL) {
    if (mg_millis() - s_timeout_ms > *(uint64_t *) c->data && (c->is_connecting || c->is_resolving)) {
      mg_error(c, "Connect timeout");
      std::cerr << "timeout\n";
      done = true;
      t_response = {-1, ""};
    }
  } else if (ev == MG_EV_CONNECT) {
    std::string endpoint = s_url, method = "POST";
    wakeupCall* data = (wakeupCall*) c->fn_data;
    if(data->flag == 0) endpoint += "/getuniqueid", method = "GET";
    if(data->flag == 1) endpoint += "/getwork";
    if(data->flag == 2) endpoint += "/returnwork";
    if(data->flag == 3) endpoint += "/trickle";
    mg_str host = mg_url_host(endpoint.c_str());
    // Send request
    int content_length = data->message.size();
    if(method == "GET") assert(content_length == 0);
    mg_printf(c,
              "%s %s HTTP/1.1\nHost: %.*s\nContent-Type: octet-stream\nContent-Length: %d\n\n",
              method.c_str(), mg_url_uri(endpoint.c_str()), (int) host.len, host.buf, content_length);
    mg_send(c, data->message.data(), content_length);
    std::cerr << "sent to " << endpoint << " " << content_length << " bytes\n";
  } else if (ev == MG_EV_HTTP_MSG) {
    mg_http_message* hm = (mg_http_message*) ev_data;
    // WARN << "got " << hm->body.len << "bytes\n";
    if((*(int *) (c->data+8)) == -1) { c->is_draining = 1; return; }
    (*(int *) (c->data+8)) = -1;
    // Response is received. Print it
    t_response = {0, _mg_str_to_stdstring(hm->body)};
    // c->is_draining = 1;
    std::cerr << "latency: " << mg_millis() - (*(uint64_t *) c->data) << " ms, repsonse len: "<<_mg_str_to_stdstring(hm->body).size()<<'\n';
    wakeupCall* data = (wakeupCall*) c->fn_data;
    // if(data->flag == 0)
      done = true;

  } else if (ev == MG_EV_ERROR) {
    std::cerr << "error\n";
    t_response = {-1, ""};
    done = true;
  }
}

void graceful_exit(mg_mgr* mgr) {
  mg_mgr_free(mgr);
  exit(0);
}

searchTree* T;

struct A2BUnit { int onx, side, idx; };
moodycamel::BlockingConcurrentQueue<A2BUnit> A2B;
std::map<int, std::vector<std::vector<std::vector<halfrow>>>> staging;
std::map<int, int> remaining;

struct B2AUnit { uint64_t row; int response; A2BUnit fa; };
moodycamel::BlockingConcurrentQueue<B2AUnit> B2A;

int nodesPending = 0;

int enqTreeNode(int onx) {
  for(int x=0; x<2; x++)
    for(int j=0; j<T->a[onx].n[x]; j++)
      A2B.enqueue({onx, x, j}), remaining[onx]++;
  staging[onx] = std::vector<std::vector<std::vector<halfrow>>>(4, std::vector<std::vector<halfrow>>(2));
  nodesPending++;
  return onx;
}

void betaUniverse() {
  A2BUnit nx;
  while (A2B.wait_dequeue(nx), nx.onx != -1) {
    int dep = T->a[nx.onx].depth;
    std::vector<uint64_t> h = T->getState(nx.onx, nx.side, nx.idx);
    genNextRows(h, dep, l4h, nx.side ? enforce2 : enforce,
                nx.side ? remember2 : remember, [&](uint64_t x) {
                  B2A.enqueue({x, dep + 1, nx});
                });
    B2A.enqueue({0, -1, nx});
  }
  //   nx = searchQueue.front();
  //   searchQueue.pop();
  //   int dep = T->a[nx.onx].depth;
  //   std::vector<uint64_t> h = T->getState(nx.onx, nx.side, nx.idx);
  //   genNextRows(h, dep, l4h, nx.side ? enforce2 : enforce,
  //               nx.side ? remember2 : remember, [&](uint64_t x) {
  //                 backQueue.push({x, dep + 1, nx});
  //               });
  //   backQueue.push({0, -1, nx});
  //   // while(!searchQueue.size() && toEnq.size() && ((t2 - t1).count() < 1 * 3600 * 1e9)) {
  //   //   enqTreeNode(toEnq.front());
  //   //   toEnq.pop();
  //   // }
  // }
}

int main(int argc, char* argv[]) {
  if (argc != 4) {
    std::cerr << "usage: " << argv[0] << " host contributorname nthreads\n";
    return 1;
  }
  s_url += argv[1];
  std::string cid = argv[2];
  int threadcount = std::stoi(argv[3]);
  mg_mgr mgr;
  mg_mgr_init(&mgr);
  // mg_wakeup_init(&mgr);  // Initialise wakeup socket pair
  auto woke_and_wait = [&](const wakeupCall& s) {
    // woker(mgr, conn, s);
    wakeupCall* data = new wakeupCall {s};
    done = false;
    mg_http_connect(&mgr, s_url.c_str(), handler_httpconn, data);
    auto stT = mg_millis();
    while(!done) {
      mg_mgr_poll(&mgr, 100);
      if(mg_millis() > stT + 60*1000ul)
        graceful_exit(&mgr);
    }
    if(t_response.flag != 0)
      graceful_exit(&mgr);
    // std::cerr << "got a response\n";
    delete data;
    return t_response.message;
  };
  int wsid;
  {
    std::istringstream ix(woke_and_wait({0, ""}));
    ix >> wsid;
    assert(!ix.fail());
  }
  int wuid = -1;
  auto actualgetWork = [&]() {
    int amnt = 0;
    while(1) {
      std::string v = std::to_string(wsid) + " 1 1";
      std::istringstream ix(woke_and_wait({1, v}), std::ios::binary);
      // WARN << ix.str().size() << "bytes: "; hexdump(ix.str().substr(0, 32));
      int amnt; readInt(amnt, ix);
      if(amnt == 0) {
        int cnt = 15000 + rand() % 5000;
        std::this_thread::sleep_for(std::chrono::milliseconds(cnt));
        continue;
      }else {
        assert(amnt == 1);
        std::string p;
        readInt(wuid, ix);
        readString(p, ix);
        return p;
      }
    }
  };
  while(1) {
    staging.clear();
    remaining.clear();
    std::istringstream ix(actualgetWork(), std::ios::binary);
    // std::cerr << "len: " << ix.str().size() << '\n';
    // hexdump(ix.str().substr(0, 16));
    nodesPending = 0;
    T = loadf(ix, "loadWorkunit");
    int cnt = 0;
    for(int i = 0; i < T->treeSize; i++) {
      if (T->a[i].n[0] == 0 || T->a[i].n[1] == 0)
        T->a[i].tags &= ~TAG_QUEUED;
      if (T->a[i].tags & TAG_QUEUED)
      cnt += (T->a[i].n[0] + T->a[i].n[1]);
    }
    if(cnt >= 1000) {
      std::ostringstream response(std::ios::binary);
      writeInt(1, response);
      writeInt(wsid, response);
      writeInt(wuid, response);
      writeInt(0, response);
      writeString(cid, response);
      woke_and_wait({2, response.str()});
      delete T;
      continue;
    }
    for (int i = 0; i < T->treeSize; i++) {
      if (T->a[i].n[0] == 0 || T->a[i].n[1] == 0)
        T->a[i].tags &= ~TAG_QUEUED;
      if (T->a[i].tags & TAG_QUEUED)
        enqTreeNode(i);
    }
    assert(staging.size() == 1);
    std::queue<int> toEnq;
    auto process = [&](B2AUnit x) {
      int id = x.fa.onx, depth = T->a[x.fa.onx].depth + 1;
      if (x.response == -1) {
        if(!--remaining[id]) {
          {
            auto& w = staging[id];
            for(int v=0; v<4; v++)
              if(sz(w[v][0]) && sz(w[v][1])) // <- Pruning is reduced to one literal line
                toEnq.push(T->newNode({v, depth, TAG_QUEUED, id}, w[v]));
          }
          T->a[x.fa.onx].tags &= ~TAG_QUEUED;
          staging.erase(staging.find(id));
          nodesPending--;
        }
      } else {
        if ((depth - 1) / p < sz(filters) && ((x.row & filters[(depth - 1) / p]) != x.row)) {
          WARN << x.row << ' ' << filters[(depth - 1) / p] << '\n';
          assert(0);
          // cout << "[1NFO] NOD3 1GNOR3D" << endl;
        } else
          staging[id][CENTER_ID(x.row)][x.fa.side].push_back({x.row, x.fa.idx});
      }
    };
    std::vector<std::thread> universes;
    for (int i = 0; i < threadcount; i++)
      universes.emplace_back(betaUniverse);
    B2AUnit nx;
    int cx = 0;
    auto t1 = std::chrono::high_resolution_clock::now();
    while (B2A.wait_dequeue(nx), 1) {
      process(nx);
      cx++;
      if(cx % 512 == 0) {
    auto t2 = std::chrono::high_resolution_clock::now();
        if((t2 -t1).count() > 20*1e9) {
          std::string v = std::to_string(wsid) + " 1 0";
          std::istringstream ix(woke_and_wait({1, v}), std::ios::binary);
          int amnt; readInt(amnt, ix);
          assert(amnt == 0);
          t1 = t2;
        }
      }
      if (!nodesPending)
        break;
    }
    for (int i = 0; i < threadcount; i++)
      A2B.enqueue({-1, -1, -1});
    for (std::thread &x : universes)
      x.join();
    std::ostringstream fx(std::ios::binary);
    uint64_t cksum = T->dumpWorkUnitResponse(fx);
    writeInt(cksum, fx);
    // dumpf(fx, "dumpWorkunitResponse", T);
    std::ostringstream response(std::ios::binary);
    writeInt(1, response);
    writeInt(wsid, response);
    writeInt(wuid, response);
    writeInt(1, response);
    writeString(cid, response);
    writeString(fx.str(), response);
    woke_and_wait({2, response.str()});
    delete T;
  }
  // std::cerr << tree.treeSize << std::endl;
  // std::cerr << toEnq.size() << std::endl;
  return 0;
}
