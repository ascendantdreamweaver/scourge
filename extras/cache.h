#pragma once

#include <filesystem>

#include "tkrzw/tkrzw_dbm_hash.h"

// Implements Cache: int - std::string KV map
// req. concurrent

class Cache {
public:
  Cache(std::string fn) {
    if (std::filesystem::exists(fn)) {
      std::filesystem::remove(fn);
    }
    assert(dbm.Open(fn, true, tkrzw::File::OPEN_TRUNCATE) == tkrzw::Status::SUCCESS);
  }
  ~Cache() {
    dbm.Close();
  }
  std::string get(int key) {
    // WARN << "get " << key << '\n';
    std::string res;
    assert(dbm.Get(std::to_string(key), &res) == tkrzw::Status::SUCCESS);
    // hexdump(res.substr(0, 16));
    return res;
  }
  void set(int key, const std::string& val) {
    // WARN << "save " << key << '\n';
    assert(dbm.Set(std::to_string(key), val) == tkrzw::Status::SUCCESS);
    // std::string res;
    // assert(dbm.Get(std::to_string(key), &res) == tkrzw::Status::SUCCESS);
  }
  void erase(int key) {
    dbm.Remove(std::to_string(key));
  }
private:
  tkrzw::HashDBM dbm;
};