#include "cadical/src/cadical.hpp"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <sstream>
#include <vector>

using std::max;
using std::min;

#include "debug.h"
#include "globals.h"
#include "logic.h"
#include "satlogic.h"
#include "searchtree.h"

searchTree* T;

struct A2BUnit { int onx, side, idx; };
std::queue<A2BUnit> searchQueue;
std::map<int, std::vector<std::vector<std::vector<halfrow>>>> staging;
std::map<int, int> remaining;

struct B2AUnit { uint64_t row; int response; A2BUnit fa; };

int enqTreeNode(int onx) {
  for(int x=0; x<2; x++)
    for(int j=0; j<T->a[onx].n[x]; j++)
      searchQueue.push({onx, x, j}), remaining[onx]++;
  staging[onx] = std::vector<std::vector<std::vector<halfrow>>>(4, std::vector<std::vector<halfrow>>(2));
  return onx;
}

int main(int argc, char* argv[]) {
  auto t1 = std::chrono::high_resolution_clock::now();
  assert(argc == 3);
  std::ifstream fin(argv[1], std::ios::in | std::ios::binary);
  std::queue<int> toEnq;
  auto process = [&](B2AUnit x) {
    int id = x.fa.onx, depth = T->a[x.fa.onx].depth + 1;
    if (x.response == -1) {
      if(!--remaining[id]) {
        {
          auto& w = staging[id];
          for(int v=0; v<4; v++)
            if(sz(w[v][0]) && sz(w[v][1])) // <- Pruning is reduced to one literal line
              toEnq.push(T->newNode({v, depth, TAG_QUEUED, id}, w[v]));
        }
        T->a[x.fa.onx].tags &= ~TAG_QUEUED;
        staging.erase(staging.find(id));
      }
    } else {
      if ((depth - 1) / p < sz(filters) && ((x.row & filters[(depth - 1) / p]) != x.row)) {
        WARN << x.row << ' ' << filters[(depth - 1) / p] << '\n';
        assert(0);
        // cout << "[1NFO] NOD3 1GNOR3D" << endl;
      } else {
        halfrow sol; sol.v(x.fa.side, x.row); sol.asc(x.fa.idx);
        staging[id][CENTER_ID(x.row)][x.fa.side].push_back(sol);
      }
    }
  };
  int cnt; readInt(cnt, fin);
  std::ofstream fout(argv[2], std::ios::out | std::ios::binary);
  writeInt(cnt, fout);
  int hrc = 0;
  for(int idx=0; idx<cnt; idx++) {
    staging.clear();
    remaining.clear();
    int wuid; readInt(wuid, fin);
    std::string tx; readString(tx, fin);
    std::istringstream ix(tx, std::ios::in | std::ios::binary);
    // std::cerr << "len: " << ix.str().size() << '\n';
    // hexdump(ix.str().substr(0, 16));
    T = loadf(ix, "loadWorkunit");
    for (int i = 0; i < T->treeSize; i++) {
      if (T->a[i].n[0] == 0 || T->a[i].n[1] == 0)
        T->a[i].tags &= ~TAG_QUEUED;
      if (T->a[i].tags & TAG_QUEUED)
        enqTreeNode(i);
    }
    assert(staging.size() == 1);
    std::queue<int> toEnq;
  A2BUnit nx;
  while (searchQueue.size()) {
    nx = searchQueue.front();
    searchQueue.pop();
    int dep = T->a[nx.onx].depth;
    std::vector<uint64_t> h = T->getState(nx.onx, nx.side, nx.idx);
    genNextRows(h, dep, l4h, nx.side ? enforce2 : enforce,
                nx.side ? remember2 : remember, [&](uint64_t x) {
                  process({x, dep + 1, nx});
                });
    process({0, -1, nx});
    // hrc++;
    // if(hrc % 128 == 0) INFO << hrc << ' ' << hrc/(clock()/1.0e6) << '\n';
  }
    std::ostringstream fx(std::ios::out | std::ios::binary);
    uint64_t cksum = T->dumpWorkUnitResponse(fx);
    writeInt(cksum, fx);
    writeInt(wuid, fout);
    writeString(fx.str(), fout);
    delete T;
  }
  writeString("done", fout);

  return 0;
}