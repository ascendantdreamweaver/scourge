#!/bin/bash
cd "$(dirname "${BASH_SOURCE[0]}")"
# g++ -o terezi_core_SAT terezi_core.cpp cadical/build/libcadical.a -O3 -lpthread -Wall -Wextra -pedantic -std=c++23 -static -DUSE_SAT
g++ -c mongoose.c -O3 -D MG_MAX_RECV_SIZE=33554432
g++ -o terezi_core terezi_core.cpp cadical/build/libcadical.a -O3 -lpthread -Wall -Wextra -pedantic -std=c++23 -static
g++ -o vriska_core vriska_core.cpp cadical/build/libcadical.a -O3 -Wall -Wextra -pedantic -std=c++23 -static
# g++ -o terezi terezi.cpp mongoose.o -O3 -L/usr/local/lib -ltkrzw -llzma -lzstd -lz -lstdc++ -lrt -latomic -lpthread -lm -lc -Wall -Wextra -pedantic -std=c++23 -D MG_MAX_RECV_SIZE=33554432 -DCOMPACT
# g++ -o vriska vriska.cpp cadical/build/libcadical.a mongoose.o -O3 -Wall -Wextra -pedantic -std=c++23 -static -D MG_MAX_RECV_SIZE=33554432
g++ -o smallcollector smallcollector.cpp cadical/build/libcadical.a mongoose.o -O3 -Wall -Wextra -pedantic -std=c++23 -static

g++ -std=c++23 -o terezi terezi.cpp -O3 -Wall -Wextra -pedantic \
     -lboost_system -lboost_thread -lboost_filesystem -lboost_coroutine -lboost_context \
     -lpthread -DCOMPACT -DLARGETREE

g++ -o vriska-static vriska.cpp cadical/build/libcadical.a -O3 -Wall -Wextra -pedantic -std=c++23 -static


# g++ -o binary_conn_A binary_conn_A.cpp mongoose.o -O3 -Wall -Wextra -pedantic -std=c++23 -static
# g++ -o binary_conn_B binary_conn_B.cpp mongoose.o -O3 -Wall -Wextra -pedantic -std=c++23 -static

echo "Done"