#pragma once

#include "logic.h"
#include "searchtree.h"

namespace searchlogic {
// State, for admin
std::string banner = "Welcome to Scourge v2.4\n";
std::string longgestPartial;
std::string oscillatorComplete;

std::queue<int> pendingOutbound_uncached;

searchTree* T = nullptr;

std::mutex searchtree_mutex;
}; // namespace searchlogic

void adminConsoleBroadcast(const std::string& s);


namespace searchlogic {
void emit(std::vector<uint64_t> mat, bool compl3t3) {
  std::stringstream fxx;
  static int bdep = 0;
  if (!compl3t3) {
    if (sz(mat) <= bdep)
      return;
    bdep = std::max(bdep, sz(mat));
  } else
    INFO << "[[OSC1LL4TOR COMPL3T3!!!]]\n";
  std::string rle;
  int cnt = 0, cur = 0;
  auto f = [&](char x) {
    if (x != cur) {
      (cnt >= 2) ? rle += std::to_string(cnt) : "";
      (cnt >= 1) ? rle += (char)cur : "";
      cnt = 0, cur = x;
    }
    cnt++;
  };
  for (int r = 0; r * p < sz(mat); r++) {
    r ? f('$') : void();
    for (int x = r * p; x < sz(mat) && x < (r + 1) * p; x++) {
      for (int j = 0; j < width; j++)
        f("bo"[!!(mat[x] & (1ull << j))]);
      f('b'), f('b'), f('b');
    }
  }
  f('!');
  (compl3t3 ? INFO : STATUS) << "x = 0, y = 0, rule = B3/S23\n" + rle + '!' << '\n';
  fxx << "<pre style='white-space: pre-wrap; word-break:break-all'><code>";
  fxx << "x = 0, y = 0, rule = B3/S23\n" + rle + "!</code></pre>" << std::endl;
  if(compl3t3) {
    oscillatorComplete += fxx.str();
    std::ofstream fx("osc-complete");
    fx<<oscillatorComplete<<std::endl; fx.flush(); fx.close();
  }
  else longgestPartial = fxx.str();
  adminConsoleBroadcast(fxx.str());
}

// New connection initialization
void adminConsoleInitConn(auto&& callback) {
  callback({1, banner});
  if(!longgestPartial.empty()) {
    callback({1, longgestPartial});
  }
  if(!oscillatorComplete.empty()) {
    callback({1, oscillatorComplete});
  }
}

// List of supported search-specific commands
std::string adminConsoleCommands = "init loadsave dumpsave treestats";

// Handle these commands
void adminConsoleCommandHandler(std::string com, std::istringstream& s, auto&& callback) {
  if(com == "init") {
    std::ifstream _f("initform.html");
    std::ostringstream f; f << _f.rdbuf();
    callback({1, f.str()});
  } else if(com == "actual-init") {
    // P W SYM L4H BTH
    const std::lock_guard<std::mutex> lock(searchtree_mutex);
    std::string options;
    if(T != nullptr) {
      callback({1, "failed, tree is already initialized"});
      goto fail;
    }
    T = new searchTree;
    // TODO: assumes stator = 0
    s >> p >> width >> sym >> l4h >> bthh;
    calculateMasks(bthh);
    if(sym != 0 && sym != 1) {
      callback({1, "failed, unsupported symmetry"});
      return;
    }
    for (int i = 0; i < 2 * p; i++) {
      uint64_t x = 0; std::string row;
      s >> row;
      if(sz(row) != width)  {
        callback({1, "failed, bad row length"});
        goto fail;
      }
      for (int j = 0; j < width; j++)
        if (row[j] == 'o')
          x |= (1ull << j);
      halfrow _A; _A.v(0, SIDE0_ID(x)); _A.asc(0);
      halfrow _B; _B.v(1, SIDE1_ID(x)); _B.asc(0);
      int ff = T->newNode({CENTER_ID(x), // Center
                    i+1, // Depth
                    (i == 2*p-1) ? TAG_QUEUED : 0, // No tags
                    i-1, // Ascendant
                    }, {{_A}, {_B}});
      // tree[newNode()]={x, short(i+1), 0, i-1, 0, 'd'};
    } s>>options;
    if (options[0] == 'y' || options[0] == 'Y') {
      int filterrows; s>>filterrows;
      for (int i = 0; i < filterrows; i++) {
        uint64_t x = 0; std::string filter; s >> filter;
        if(sz(filter) != width) {
          callback({1, "failed, bad filter length, must be w"});
          goto fail;
        }
        for (int j = 0; j < width; j++)
          if (filter[j] == 'o')
            x |= (1ull << j);
        filters.push_back(x);
      }
      for(int idx=0; idx<2; idx++){
        int cnt; s>>cnt;
        leftborder[idx] = std::vector<uint64_t>(cnt);
        for(int i=0; i<cnt; i++){
          std::string t; s>>t;
          if(sz(t) != p)  {
            callback({1, "failed, bad fix length, must be p"});
            goto fail;
          }
          for(int j=0; j<p; j++) if(t[j] == 'o') leftborder[idx][i] |= (1ull << j);
        }
      }
      exInitrow = std::vector<uint64_t>(2*p);
      for(int i=0; i<2*p; i++) {
        uint64_t x = 0; std::string row;
        s >> row;
        if(sz(row) != width)  {
          callback({1, "failed, bad row length"});
          goto fail;
        }
        for (int j = 0; j < width; j++)
          if (row[j] == 'o')
            x |= (1ull << j);
        exInitrow[i] = x;
      }
    }
    assert(T->treeSize == 2*p);
    {
      std::ostringstream c(std::ios::out | std::ios::binary);
      dumpf(c, "dumpWorkunit", T, 2*p-1);
      pendingOutbound_uncached.push(2*p-1);
      callback({1, "successfully parsed input:\n<pre><code>" + s.str() + "</code></pre>"});
    }
    if(s.fail()) {
      callback({1, "unknown parsing failure"});
      exit(0);
      // goto fail;
    }
    fail:; delete T;
  } else if (com == "loadsave") {
    const std::lock_guard<std::mutex> lock(searchtree_mutex);
    if(T != nullptr) {
      callback({1, "failed, tree is already initialized"});
    } else {
      std::string fn; s>>fn;
      std::ifstream dump(fn, std::ios::in | std::ios::binary);
      T = loadf(dump, "loadTree");
      int cnt = 0;
      for(int i=0; i<T->treeSize; i++)
        if(T->a[i].tags & TAG_QUEUED) { // TOCHECK
          // cref [TODO: Nonephemeral node process here]
          if(T->a[i].tags & TAG_REMOTE_QUEUED)
            T->a[i].tags ^= TAG_REMOTE_QUEUED;
          cnt++;
          pendingOutbound_uncached.push(i);
        }
      callback({1, "loaded " + std::to_string(T->treeSize) + " nodes from "+fn+
        " and queued "+std::to_string(cnt)+" nodes"});
      int dropped = T->dropNodes();
      callback({1, "dropped "+std::to_string(dropped)+" nodes"});
    }
  } else if(com == "dumpsave") {
    const std::lock_guard<std::mutex> lock(searchtree_mutex);
    std::string fn; s>>fn;
    std::ofstream dump(fn, std::ios::out | std::ios::binary);
    dumpf(dump, "dumpTree", T);
    // TODO: Nonephemeral node process here
    callback({1, "saved " + std::to_string(T->treeSize) + " nodes to "+fn});
  } else if(com == "treestats") {
    callback({1, "crunching tree stats..."});
    const std::lock_guard<std::mutex> lock(searchtree_mutex);
    std::ostringstream fx;
    int maxdep = 0, qcnt = 0; //, dup = 0;
    long long hrs = 0, qhrs = 0;
    uint64_t impl = 0, qimpl = 0;
    std::vector<std::pair<std::pair<int, long long>, std::string>> conIdx;
    for(auto s:T->contributors) conIdx.push_back({{0,0}, s});
    for(int i=0; i<T->treeSize; i++) {
      maxdep = std::max(maxdep, (int)T->a[i].depth);
      hrs += T->a[i].n[0] + T->a[i].n[1];
      impl += 1ll * T->a[i].n[0] * T->a[i].n[1];
      if(T->a[i].tags & TAG_QUEUED)
        qcnt++, qhrs += T->a[i].n[0] + T->a[i].n[1], qimpl += 1ll * T->a[i].n[0] * T->a[i].n[1];
      // dup += (tree[i].state == '2');
    }
    std::vector<int> ongoing(maxdep+1), total(maxdep+1);
    for(int i=0;i<T->treeSize; i++) {
      total[T->a[i].depth]++;
      if(T->a[i].tags & TAG_QUEUED) ongoing[T->a[i].depth]++;
      else if(i>=2*p) {
        if(T->a[i].cid >= conIdx.size()) continue;
        auto& u = conIdx[T->a[i].cid].first;
        u.first++, u.second+=T->a[i].n[0] + T->a[i].n[1];
      }
    }
    fx << std::format("{} nodes {} &frac12;rs ({} q {} q&frac12;rs) <br>", T->treeSize, hrs, qcnt, qhrs);
    fx << std::format("{:.1f}K implied ({:.1f}K q)<br>", impl/1000.0, qimpl/1000.0);
    fx << std::format("max depth {}<br>", maxdep);
    fx << "profile";
    for(int i=0; i<=maxdep; i++){
      if(ongoing[i])fx<<' '<<ongoing[i]<<'/'<<total[i];
      else fx<<' '<<total[i];
    }
    fx<<"<br>contributors<br>";
    std::sort(conIdx.begin(), conIdx.end()); std::reverse(conIdx.begin(), conIdx.end());
    for(auto& [cnt, id]:conIdx)fx<<id<<": "<<cnt.first<<" nodes " << cnt.second << " &frac12;rs<br>";
    fx<<longgestPartial;
    if(oscillatorComplete.size())fx<<oscillatorComplete;
    callback({1, fx.str()});
  }
  else {
    callback({1, "unknown command"});
  }
}

bool getWork(int& workunitId, int& workunitWeight, std::string& workunitData) {
  const std::lock_guard<std::mutex> lock(searchtree_mutex);
  if(T == nullptr) return false;
  if(pendingOutbound_uncached.empty()) return false;
  workunitId = pendingOutbound_uncached.front();
  pendingOutbound_uncached.pop();
  std::ostringstream c(std::ios::out | std::ios::binary);
  dumpf(c, "dumpWorkunit", T, workunitId);
  workunitData = c.str();
  workunitWeight = T->a[workunitId].n[0] + T->a[workunitId].n[1];
  // INFO << workunitWeight << '\n';
  return true;
}

void returnWork(int workunitId, const std::string& workunitData, const std::string& contributor) {
  const std::lock_guard<std::mutex> lock(searchtree_mutex);
  if(!(T->a[workunitId].tags & TAG_QUEUED)) return;
  std::istringstream b(workunitData, std::ios::in | std::ios::binary);
  if(!T->contributorIDs.contains(contributor)) {
    T->contributorIDs[contributor] = T->contributors.size();
    T->contributors.push_back(contributor);
  }
  T->a[workunitId].cid = T->contributorIDs[contributor];
  int oldTreeSize = T->treeSize;
  uint64_t cksum = T->loadWorkUnitResponse(b);
  uint64_t expected_cksum = 1025; readInt(expected_cksum, b);
  assert(cksum == expected_cksum); // failure would be irrecoverable so whatever
  for(int idx=oldTreeSize; idx<T->treeSize; idx++) {
    if(T->a[idx].tags & TAG_QUEUED) { // worker did not complete this node
      pendingOutbound_uncached.push(idx);
    } else { // completed, in addition to workunitId
      T->a[idx].cid = T->contributorIDs[contributor];
    }
    // TODO: maybe delegate this to a different thread
    int ixs[2], isComplete[2]; ixs[0] = ixs[1] = isComplete[0] = isComplete[1] = 0;
    for(int x=0; x<2; x++)
      for (int i = 0; i < T->a[idx].n[x]; i++) {
        const std::vector<uint64_t> st = T->getState(idx, x, i);
        if (Compl3t34bl3(st, T->a[idx].depth, T->a[idx].depth % p, x ? enforce2 : enforce)) {
          ixs[x] = i, isComplete[x] = 1;
          break;
        }
      }
    std::vector<uint64_t> marge = T->getState(idx, 0, ixs[0], true);
    std::vector<uint64_t> marge2 = T->getState(idx, 1, ixs[1], true);
    for (int x = 0; x < sz(marge); x++)
      marge[x] |= marge2[x];            
    emit(marge, isComplete[0] && isComplete[1]);
  }
}

void relinquishWork(int workunitId) {
  const std::lock_guard<std::mutex> lock(searchtree_mutex);
  if(!(T->a[workunitId].tags & TAG_QUEUED)) {
    WARN << "attempted to relinquish unqueued unit " << workunitId << '\n';
    return;
  }
  if(T->a[workunitId].tags & TAG_REMOTE_QUEUED)
    T->a[workunitId].tags ^= TAG_REMOTE_QUEUED;
  else
    WARN << "relinquishing unit " << workunitId << " which is not remote queued\n";
  pendingOutbound_uncached.push(workunitId);
}

std::string autosave1 = "autosave-odd.txt", autosave2 = "autosave-even.txt";

void every256Loops(int conns, int pendingOutSize, int pendingInSize) {
  const std::lock_guard<std::mutex> lock(searchtree_mutex);
  if(T == nullptr) return;
  std::ostringstream res;
  auto now_tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  res << std::put_time(std::gmtime(&now_tt), "%c %Z") << " conn#: " << conns;
  res << " q#: "<<pendingOutSize+pendingOutbound_uncached.size() << " pending#: "<<pendingInSize;
  adminConsoleBroadcast(res.str());
  static int idx = 0;
  idx++;
  if(idx % 8 == 0) {
    std::ofstream dump(autosave1, std::ios::out | std::ios::binary);
    dumpf(dump, "dumpTree", T);
    dump.flush(); dump.close();
    std::ostringstream res2; res2 << "autosaved to "<<autosave1;
    swap(autosave1, autosave2);
    adminConsoleBroadcast(res2.str());
  }
}
}; // namespace searchlogic
