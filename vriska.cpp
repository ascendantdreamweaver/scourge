#include "cadical/src/cadical.hpp"

#include "cqueue/bcq.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <queue>
#include <random>
#include <set>
#include <sstream>
#include <vector>
#include <thread>
#include <vector>
#include <sstream>

using std::max;
using std::min;

#include "debug.h"
#include "globals.h"
#include "logic.h"
#include "satlogic.h"
#include "searchtree.h"

#include "httplib.h"

std::string s_url = "http://";
const uint64_t s_timeout_ms = 100000;  // Connect timeout in milliseconds

struct wakeupCall {
  int flag = 0; // 0 - send getuniqueid, 1 - send getwork2, 2 - send returnwork, 3 - send trickle
  std::string message;
};

std::string send_http_request(const wakeupCall& call, httplib::Client& cli) {
  cli.set_connection_timeout(s_timeout_ms / 1000, 0); // seconds
  
  std::string endpoint;
  switch(call.flag) {
    case 0: endpoint = "/getuniqueid"; break;
    case 1: endpoint = "/getwork2"; break;
    case 2: endpoint = "/returnwork"; break;
    case 3: endpoint = "/trickle"; break;
    default: throw std::runtime_error("Invalid flag");
  }
  
  auto start = std::chrono::steady_clock::now();
  httplib::Result res;
  
  if(call.flag == 0) {
    std::cerr << "sent to " << endpoint.c_str() << " GET\n";
    res = cli.Get(endpoint.c_str());
  } else {
    std::cerr << "sent to " << endpoint.c_str() << " " << call.message.size() << " bytes\n";
    httplib::Headers headers = {
      {"Content-Type", "octet-stream"}
    };
    res = cli.Post(endpoint.c_str(), headers, call.message, "octet-stream");
  }

  if(!res || res->status != 200) {
    std::cerr << "HTTP request failed\n";
    if(res) {
      std::cerr << "status: " << res->status << '\n';
      std::cerr << "body: " << res->body << '\n';
    }
    return "";
  }
  
  auto end = std::chrono::steady_clock::now();
  auto latency = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
  
  std::cerr << "latency: " << latency << " ms, response len: " << res->body.size() << '\n';
  return res->body;
}

void graceful_exit() {
  exit(0);
}


struct A2BUnit { int onx, side, idx; };
struct B2AUnit { uint64_t row; int response; A2BUnit fa; };

std::string run(std::string input) {
  std::istringstream ix(input, std::ios::binary);
  // std::cerr << "len: " << ix.str().size() << '\n';
  // hexdump(ix.str().substr(0, 16));
  int nodesPending = 0;
  searchTree* T = loadf(ix, "loadWorkunit");
  std::queue<A2BUnit> A2B;
  std::map<int, std::vector<std::vector<std::vector<halfrow>>>> staging;
  std::map<int, int> remaining;
  auto enqTreeNode = [&](int onx) {
    for(int x=0; x<2; x++)
      for(int j=0; j<T->a[onx].n[x]; j++)
        A2B.push({onx, x, j}), remaining[onx]++;
    staging[onx] = std::vector<std::vector<std::vector<halfrow>>>(4, std::vector<std::vector<halfrow>>(2));
    nodesPending++;
    return onx;
  };
  for (int i = 0; i < T->treeSize; i++) {
    if (T->a[i].n[0] == 0 || T->a[i].n[1] == 0)
      T->a[i].tags &= ~TAG_QUEUED;
    if (T->a[i].tags & TAG_QUEUED)
      enqTreeNode(i);
  }
  assert(staging.size() == 1);
  std::queue<int> toEnq;
  auto process = [&](B2AUnit x) {
    int id = x.fa.onx, depth = T->a[x.fa.onx].depth + 1;
    if (x.response == -1) {
      if(!--remaining[id]) {
        {
          auto& w = staging[id];
          for(int v=0; v<4; v++)
            if(sz(w[v][0]) && sz(w[v][1])) // <- Pruning is reduced to one literal line
              toEnq.push(T->newNode({v, depth, TAG_QUEUED, id}, w[v]));
        }
        T->a[x.fa.onx].tags &= ~TAG_QUEUED;
        staging.erase(staging.find(id));
        nodesPending--;
      }
    } else {
      if ((depth - 1) / p < sz(filters) && ((x.row & filters[(depth - 1) / p]) != x.row)) {
        WARN << x.row << ' ' << filters[(depth - 1) / p] << '\n';
        assert(0);
      } else {
        halfrow sol; sol.v(x.fa.side, x.row); sol.asc(x.fa.idx);
        staging[id][CENTER_ID(x.row)][x.fa.side].push_back(sol);
      }
    }
  };
  std::vector<std::thread> universes;
  auto t0 = std::chrono::high_resolution_clock::now();
  while (A2B.size()) {
    A2BUnit nx = A2B.front(); A2B.pop();
    int dep = T->a[nx.onx].depth;
    std::vector<uint64_t> h = T->getState(nx.onx, nx.side, nx.idx);
    genNextRows(h, dep, l4h, nx.side ? enforce2 : enforce,
                nx.side ? remember2 : remember, [&](uint64_t x) {
                  process({x, dep + 1, nx});
                });
    process({0, -1, nx});
    if (!A2B.size()) {

      break;
      if(!toEnq.size()) break;
      auto t2 = std::chrono::high_resolution_clock::now();
      if((t2 -t0).count() > 300*1e9) break;
      enqTreeNode(toEnq.front());
      toEnq.pop();
    }
  }
  std::ostringstream fx(std::ios::binary);
  uint64_t cksum = T->dumpWorkUnitResponse(fx);
  writeInt(cksum, fx);
  delete T;
  return fx.str();
}

struct pendingWorkUnit { int wuid, weight; std::string p; };
std::atomic_int pwuqSize;
moodycamel::BlockingConcurrentQueue<pendingWorkUnit> pendingWorkUnitQueue;
moodycamel::BlockingConcurrentQueue<pendingWorkUnit> pendingReturnQueue;

void worker() {
  pendingWorkUnit deq;
  while(pendingWorkUnitQueue.wait_dequeue(deq), deq.wuid != -1) {
    pwuqSize -= deq.weight;
    pendingReturnQueue.enqueue({deq.wuid, 0, run(deq.p)});
  }
}

int main(int argc, char* argv[]) {
  if (argc != 4) {
    std::cerr << "usage: " << argv[0] << " host contributorName nThreads\n";
    return 1;
  }
  s_url += argv[1];
  httplib::Client cli (s_url);  // Initialize the global client
  std::string cid = argv[2];
  int threadcount = std::stoi(argv[3]);
  int targetWeight = 10000;
  auto woke_and_wait = [&](const wakeupCall& s) {
    auto response = send_http_request(s, cli);
    if(response.empty()) {
      graceful_exit();
    }
    return response;
  };
  int wsid;
  {
    std::istringstream ix(woke_and_wait({0, ""}));
    ix >> wsid;
    assert(!ix.fail());
  }
  auto trickle = [&]() {
    std::string v = std::to_string(wsid);
    std::istringstream ix(woke_and_wait({3, v}));
    std::string r; ix >> r;
    assert(r == "OK");
  };
  std::mt19937 rng(wsid);
  auto update = [&]() {
    if(pendingWorkUnitQueue.size_approx() <= 1)
      targetWeight = std::min(10000000.0, targetWeight * 1.2);
    if(pendingWorkUnitQueue.size_approx() >= threadcount*20) 
      targetWeight = std::max(1000.0, targetWeight / 1.5);
    int cnt = pendingReturnQueue.size_approx();
    std::vector<pendingWorkUnit> toReturn(cnt);
    toReturn.resize(pendingReturnQueue.try_dequeue_bulk(toReturn.begin(), cnt));
    bool interuct = false;
    if(toReturn.size()) {
      std::ostringstream response(std::ios::binary);
      writeInt(toReturn.size(), response);
      writeInt(wsid, response);
      for(pendingWorkUnit& wu : toReturn) {
        writeInt(wu.wuid, response);
        writeInt(1, response);
        writeString(cid, response);
        writeString(wu.p, response);
      }
      woke_and_wait({2, response.str()});
      interuct = true;
    }
    int currentpwuqSize = pwuqSize;
    INFO << currentpwuqSize << ' ' << targetWeight << '\n';
    if(currentpwuqSize < targetWeight) {
      std::string v = std::to_string(wsid) + " " + std::to_string(targetWeight - currentpwuqSize);
      std::istringstream ix(woke_and_wait({1, v}), std::ios::binary);
      interuct = true;
      int amnt; readInt(amnt, ix);
      if(amnt == 0) {
        int cnt = 15000 + rng() % 5000;
        std::this_thread::sleep_for(std::chrono::milliseconds(cnt));
        return trickle();
      }else {
        for(int i=0; i<amnt; i++) {
          std::string p;
          int wuid, wt;
          readInt(wuid, ix);
          readInt(wt, ix);
          // INFO << wt << '\n';
          readString(p, ix);
          pwuqSize += wt;
          pendingWorkUnitQueue.enqueue({wuid, wt, p});
        }
      }
    }
    if(!interuct) trickle();
  };

  std::vector<std::thread> universes;
  for (int i = 0; i < threadcount; i++)
    universes.emplace_back(worker);

  // main loop
  update();
  while(1) {
    std::this_thread::sleep_for(std::chrono::milliseconds(19000 + rng() % 1000));
    update();
  }

  for (int i = 0; i < threadcount; i++)
    pendingWorkUnitQueue.enqueue({-1, 0, ""});
  for (std::thread &x : universes)
    x.join();
  return 0;
}
