#pragma once

#include "globals.h"

#include <vector>
#include <unordered_map>

namespace _searchtree {
/* u - unqueued; q - queued; d - decomissioned; c - valid completion for one
 * side */
constexpr int TAG_VALID_COMPLETION = 1;
constexpr int TAG_QUEUED = 2;
constexpr int TAG_REMOTE_QUEUED = 1<<2;
constexpr int TAG_DROPPED_HALFROWS = 1<<3;
// constexpr int TAG_INDETERMINATE_NODEID = 1<<3;
#ifdef COMPACT
// ONLY USE WHEN SYM=0!!!!!!!!
struct halfrow {
  uint32_t internal; // first 13 bits for row, last 19 bits for top 19 bits of asc
  uint8_t asc_extra; // bottom 8 bits of asc
  uint64_t v(int x) {
    uint64_t r = internal & ((1 << 13) - 1);
    // Reconstruct the full row from compressed bits
    uint64_t mask = (x == 0) ? remember : remember2;
    int start = __builtin_ctzll(mask);
    return (r << start) & mask;
  }
  uint64_t v(int x, const uint64_t& s) {
    // Compress the row into bits based on remember/remember2 mask
    uint64_t mask = (x == 0) ? remember : remember2;
    int start = __builtin_ctzll(mask);
    uint64_t compressed = s >> start;
    // Store compressed value in first 13 bits
    internal = (internal & ~((1 << 13) - 1)) | (compressed & ((1 << 13) - 1));
    return s;
  }
  int asc() {
    return ((internal >> 13) << 8) | asc_extra;
  }
  int asc(const uint64_t& s) {
    assert(s < 134217728);
    internal = (internal & ((1 << 13) - 1)) | ((s >> 8) << 13);
    asc_extra = s;
    return s;
  }
}; // 32bits per internal row
#else
struct halfrow {
  uint64_t _v; int _asc;
  uint64_t v(int x) { return _v; }
  uint64_t v(int x, const uint64_t& s) { return _v = s; }
  int asc() { return _asc; }
  int asc(const uint64_t& s) { return _asc = s; }
  halfrow() : _v(0), _asc(0) { }
};
#endif
struct node {
  uint8_t v; // 8bits (2bits effective)
  short depth; // 16bits
  int tags; // 32bits
  int asc, ch[4]; // 32*5bits
  int n[2]; // 32*2bits
  halfrow* h[2]; // 64*2bits
  int cid; // 32bits
  node(uint8_t v=0, short depth=0, int tags=0, int asc=0, int cid=0) 
    : v(v), depth(depth), tags(tags), asc(asc), cid(cid) {
    ch[0] = ch[1] = ch[2] = ch[3] = 0;
    n[0] = n[1] = 0;
    h[0] = h[1] = nullptr;
  }
  ~node() {
    if(h[0] != nullptr) delete[] h[0];
    if(h[1] != nullptr) delete[] h[1];
  }
  uint64_t dump(int id, std::ostream& f) {
    std::vector<std::unordered_map<uint64_t, int>> rowcomp(2);
    std::vector<std::vector<uint64_t>> rowlis(2);
    std::vector<int> maxasc = {1, 1};
    if(!(tags & TAG_DROPPED_HALFROWS))
      for(int x=0; x<2; x++) for(int j=0; j<n[x]; j++) {
        maxasc[x] = std::max(maxasc[x], h[x][j].asc() + 1);
        if(!rowcomp[x].contains(h[x][j].v(x)))
          rowcomp[x][h[x][j].v(x)] = rowlis[x].size(), rowlis[x].push_back(h[x][j].v(x));
      }
    arithWriteToStream({(uint64_t)id, (uint64_t)v, (uint64_t)std::max(0,asc), (uint64_t)tags, 
                        (uint64_t)rowlis[0].size(), (uint64_t)n[0], (uint64_t)maxasc[0],
                        (uint64_t)rowlis[1].size(), (uint64_t)n[1], (uint64_t)maxasc[1],
                        (uint64_t)cid},
                        {1ull<<32, 1ull<<8, 1ull<<32, 1ull<<32, 1ull<<32, 1ull<<32, 1ull<<32,
                         1ull<<32, 1ull<<32, 1ull<<32, 1ull<<32}, f);
    uint64_t cksum = id + asc;
    if(tags & TAG_DROPPED_HALFROWS)
      return cksum;
    std::vector<uint64_t> vals, maxvals;
    for(int x=0; x<2; x++) {
      for(auto r : rowlis[x])
        vals.push_back(r), maxvals.push_back(1ull<<63);
      for(int j=0; j<n[x]; j++) {
        vals.push_back(rowcomp[x][h[x][j].v(x)]), maxvals.push_back(rowlis[x].size());
        vals.push_back(h[x][j].asc()), maxvals.push_back(maxasc[x]);
        cksum = cksum * 9 + h[x][j].asc() * 3 + h[x][j].v(x);
      }
    }
    arithWriteToStream(vals, maxvals, f);
    return cksum;
  }
  uint64_t load(int& id, std::istream& f) {
    std::vector<uint64_t> header;
    arithReadFromStream(header, {1ull<<32, 1ull<<8, 1ull<<32, 1ull<<32, 1ull<<32, 1ull<<32, 1ull<<32,
                         1ull<<32, 1ull<<32, 1ull<<32, 1ull<<32}, f);
    id = header[0];
    v = header[1], asc = header[2], tags = header[3];
    if(id == 0 && asc == 0)
      asc = -1;
    n[0] = header[5], n[1] = header[8];
    cid = header[10];
    uint64_t cksum = id + asc;
    if(tags & TAG_DROPPED_HALFROWS)
      return cksum;
    int maxasc[2] = {(int)header[6], (int)header[9]}, rowlisSize[2] = {(int)header[4], (int)header[7]};
    std::vector<uint64_t> vals, maxvals;
    for(int x=0; x<2; x++) {
      for(int i=0; i<rowlisSize[x]; i++)
        vals.push_back(0), maxvals.push_back(1ull<<63);
      for(int i=0; i<n[x]; i++) {
        vals.push_back(0), maxvals.push_back(rowlisSize[x]);
        vals.push_back(0), maxvals.push_back(maxasc[x]);
      }
    }
    arithReadFromStream(vals, maxvals, f);
    std::reverse(vals.begin(), vals.end());
    std::vector<std::vector<uint64_t>> rowlis(2);
    for(int x=0; x<2; x++) {
      for(int i=0; i<rowlisSize[x]; i++)
        rowlis[x].push_back(vals.back()), vals.pop_back();
      if (h[x] != nullptr)
        delete[] h[x];
      if (n[x])
        h[x] = new halfrow[n[x]];
      for(int i=0; i<n[x]; i++) {
        uint64_t a, b;
        h[x][i].v(x, a = rowlis[x][vals.back()]), vals.pop_back();
        h[x][i].asc(b = vals.back()), vals.pop_back();
        cksum = cksum * 9 + h[x][i].asc() * 3 + h[x][i].v(x);
      }
    }
    return cksum;
  }
};
#ifdef LARGETREE
const int treeAlloc = 2097152*32;
#else
const int treeAlloc = 2097152;
#endif

struct searchTree {
  node *a;
  int depths[1000];
  int depthcnt[1000];
  int treeSize = 0;
  int wu_onx = 0;
  std::vector<std::string> contributors; std::map<std::string, int> contributorIDs;
  searchTree() { a = new node[treeAlloc]; }
  ~searchTree() { delete[] a; }
  uint64_t dumpTree(std::ostream &f) const {
    f.write("terezi", 6);
    writeInt(treeSize, f);
    uint64_t cksum = 0;
    for (int i = 0; i < treeSize; i++) 
      cksum ^= a[i].dump(i, f);
    writeInt(contributors.size(), f);
    for(auto s:contributors)
      writeString(s, f);
    f.write("pyrope", 6);
    return cksum;
  }
  uint64_t loadTree(std::istream &f) {
    for (int i = 0; i < 1000; i++)
      depthcnt[i] = depths[i] = 0;
    std::string buf(6, 0);
    f.read(buf.data(), 6);
    assert(buf == "terezi");
    int cnt[2]; cnt[0] = cnt[1] = 0;
    readInt(treeSize, f);
    uint64_t cksum = 0;
    for (int i = 0; i < treeSize; i++) {
      int ii;
      cksum ^= a[i].load(ii, f);
      assert(ii == i);
      a[i].ch[0] = a[i].ch[1] = a[i].ch[2] = a[i].ch[3] = -1;
      if (i)
        a[a[i].asc].ch[a[i].v] = i;
      for (int x = 0; x < 2; x++)
        if (a[i].n[x])
          cnt[x] += a[i].n[x];
      a[i].depth = 1 + (i ? a[a[i].asc].depth : 0);
    }
    int cs;
    readInt(cs, f);
    contributors = std::vector<std::string>(cs);
    for(int i=0; i<cs; i++)
      readString(contributors[i], f), contributorIDs[contributors[i]] = i;
    f.read(buf.data(), 6);
    assert(buf == "pyrope");
    WARN << "LO4D3D " << treeSize << " NOD3S 4ND " << cnt[0] << "+" << cnt[1] << " ROWS\n";
    for (int i = 0; i < treeSize; i++) {
      depths[a[i].depth] += !!(a[i].tags & TAG_QUEUED), depthcnt[a[i].depth]++;
    }
    return cksum;
  }
  int dropNodes() {
    int* dependency = new int[treeSize];
    std::fill(dependency, dependency + treeSize, 0);
    for (int i = treeSize - 1; i > 0; i--) {
      dependency[i] += !!(a[i].tags & TAG_QUEUED);
      dependency[a[i].asc] += dependency[i];
    }
    int cnt = 0;
    for (int i = 1; i < treeSize; i++) {
      if (!dependency[i]) {
        if(a[i].h[0] != nullptr) {
          delete[] a[i].h[0];
          a[i].h[0] = nullptr;
        }
        if(a[i].h[1] != nullptr) {
          delete[] a[i].h[1];
          a[i].h[1] = nullptr;
        }
        a[i].tags |= TAG_DROPPED_HALFROWS;
        cnt++;
      }
    }
    delete[] dependency;
    return cnt;
  }
  uint64_t loadWorkUnit(std::istream &f) {
    for (int i = 0; i < 1000; i++)
      depthcnt[i] = depths[i] = 0;
    std::string buf(6, 0);
    f.read(buf.data(), 6);
    assert(buf == "terezi");
    readInt(wu_onx, f);
    readInt(treeSize, f);
    assert(treeSize == 2 * p);
    std::vector<int> depthShit(2*p);
    for(int i=0; i<2*p; i++) readInt(depthShit[i], f);
    uint64_t cksum = 0;
    for (int i = 0; i < treeSize; i++) {
      int ii;
      cksum ^= a[i].load(ii, f);
      partialLoadOriginalNodeid[i] = ii;
      partialLoadNewNodeid[ii] = i;
      a[i].ch[0] = a[i].ch[1] = a[i].ch[2] = a[i].ch[3] = -1;
      if (i) {
        assert(partialLoadNewNodeid[a[i].asc] == i-1);
        a[i].asc = i-1;
        a[a[i].asc].ch[a[i].v] = i;
      } else a[i].asc = -1;
      a[i].depth = depthShit[i];
    }
    f.read(buf.data(), 6);
    assert(buf == "pyrope");
    assert(partialLoadOriginalNodeid[2*p-1] == wu_onx);
    return cksum;
  }
  uint64_t dumpWorkUnit(std::ostream &f, int onx) const {
    f.write("terezi", 6);
    writeInt(onx, f);
    writeInt(2*p, f);
    if(!(a[onx].tags & TAG_QUEUED)) {
      WARN << onx << " does NOT have TAG_QUEUED on dumping\n";
      a[onx].tags |= TAG_QUEUED;
    }
    if(a[onx].tags & TAG_REMOTE_QUEUED) {
      WARN << onx << " DOES have TAG_REMOTE_QUEUED on dumping\n";
      a[onx].tags ^= TAG_REMOTE_QUEUED;
    }
    a[onx].tags |= TAG_REMOTE_QUEUED;
    // WARN << onx << "has REMOTE_QUEUED now " << a[933].tags << "\n";
    std::vector<int> td;
    for(int i=0; i<2*p; i++) td.push_back(onx), onx = a[onx].asc;
    std::reverse(td.begin(), td.end());
    for(int i=0; i<2*p; i++) writeInt(a[td[i]].depth, f);
    uint64_t cksum = 0;
    for (int i = 0; i < 2*p; i++) 
      cksum ^= a[td[i]].dump(td[i], f);
    f.write("pyrope", 6);
    return cksum;
  }
  uint64_t dumpWorkUnitResponse(std::ostream &f) const {
    f.write("terezi", 6);
    writeInt(wu_onx, f);
    // WARN << "DWUR " << wu_onx << '\n';
    writeInt(treeSize, f);
    uint64_t cksum = 0;
    for (int i = 2*p; i < treeSize; i++) 
      cksum ^= a[i].dump(i, f);
    f.write("pyrope", 6);
    return cksum;
  }
  uint64_t loadWorkUnitResponse(std::istream &f) {
    std::string buf(6, 0);
    f.read(buf.data(), 6);
    assert(buf == "terezi");
    readInt(wu_onx, f);
    // WARN << "LWUR " << wu_onx << '\n';
    // WARN << "..."<<a[933].tags<<'\n';
    assert((a[wu_onx].tags & TAG_QUEUED)); // those modified by worker has no bearing on this
    // WARN << wu_onx << " check RQ " << a[wu_onx].tags << "\n";
    assert((a[wu_onx].tags & TAG_REMOTE_QUEUED));
    a[wu_onx].tags ^= TAG_QUEUED;
    a[wu_onx].tags ^= TAG_REMOTE_QUEUED;
    int xTreeSize;
    readInt(xTreeSize, f);
    uint64_t cksum = 0;
    for (int i = treeSize; i < treeSize + xTreeSize - 2*p; i++) {
      int ii;
      cksum ^= a[i].load(ii, f); assert(ii == i + 2*p - treeSize);
      if(a[i].asc < 2 * p) {
        assert(a[i].asc == 2*p-1);
        a[i].asc = wu_onx;
      } else {
        a[i].asc = a[i].asc + treeSize - 2*p;
      }
      a[i].ch[0] = a[i].ch[1] = a[i].ch[2] = a[i].ch[3] = -1;
      a[a[i].asc].ch[a[i].v] = i;
      a[i].depth = 1 + (i ? a[a[i].asc].depth : 0);
    }
    f.read(buf.data(), 6);
    assert(buf == "pyrope");
    treeSize += xTreeSize - 2*p;
    return cksum;
  }
  
  void flushTree() {}
  int newNode(node s, std::vector<std::vector<halfrow>> bufs) {
    assert(s.asc != -1 || treeSize == 0);
    s.ch[0] = s.ch[1] = s.ch[2] = s.ch[3] = -1;
    a[treeSize] = s;
    for(int x=0; x<2; x++){
      a[treeSize].n[x] = bufs[x].size();
      if (a[treeSize].n[x]) {
        a[treeSize].h[x] = new halfrow[a[treeSize].n[x]];
        std::copy(bufs[x].begin(), bufs[x].end(), a[treeSize].h[x]);
      }
    }
    if (s.asc != -1 && a[s.asc].ch[s.v] != -1) {
      WARN << "F4T4L: N3W NOD3 " << treeSize << " DUPLIC4T3S KNOWN S3QU3NC3\n";
      assert(a[s.asc].ch[s.v] == -1);
    }
    if (s.asc != -1)
      a[s.asc].ch[s.v] = treeSize;
    depthcnt[s.depth]++;
    if (s.tags & TAG_QUEUED)
      depths[s.depth]++;
    return treeSize++;
  }
  void filterNode(int onx, std::vector<std::set<int>>& passes) {
    for(int v=0; v<4; v++) assert(a[onx].ch[v] == -1);
    INFO << "node " << onx << " (seq='" << brief(onx) << "'): " << a[onx].n[0] << "+" << a[onx].n[1] << " -> ";
    for(int x=0; x<2; x++){
      halfrow* nh = nullptr;
      if(passes[x].size()) {
        nh = new halfrow[passes[x].size()];
        std::vector<int> idxs(passes[x].begin(), passes[x].end());
        for(int i=0; i<(int)passes[x].size(); i++)
          nh[i] = a[onx].h[x][idxs[i]];
      }
      if(a[onx].h[x] != nullptr) delete[] a[onx].h[x];
      a[onx].n[x] = passes[x].size(), a[onx].h[x] = nh;
    }
    INFO << a[onx].n[0] << "+" << a[onx].n[1] << " ½rs\n";
  }
  std::vector<uint64_t> getState(int onx, int side, int idx, bool full = 0) {
    std::vector<uint64_t> mat;
    while (full ? onx != -1 : (mat.size() != 2 * p))
      mat.push_back(a[onx].h[side][idx].v(side)), idx = a[onx].h[side][idx].asc(), onx = a[onx].asc;
    reverse(mat.begin(), mat.end());
    return mat;
  }
  std::string brief(int onx) {
    std::string res;
    while(onx != -1)
      res += a[onx].v + '0', onx = a[onx].asc;
    reverse(res.begin(), res.end());
    return std::string(res.begin() + 2*p, res.end());
  }
  int getWidth(int i, int side, int idx) {
    uint64_t bito = 0;
    for (uint64_t x : getState(i, side, idx))
      bito |= x;
    return 64 - __builtin_clzll(bito) - __builtin_ctzll(bito);
  }
};
}; // namespace _searchtree
using namespace _searchtree;

searchTree* loadf(std::istream &f, std::string mode) {
  // BENCHMARK(load)
  if(paramsLoaded++) {
    int a, _;
    for(int i=0; i<7; i++) readInt(_, f);
    readInt(a, f);
    for(int i=0; i<a; i++) readInt(_, f);
    for(int s=0; s<2; s++) {
      int sz; readInt(sz, f);
      std::vector<uint64_t> b;
      arithReadFromStream(b, std::vector<uint64_t>(sz, 1<<p), f);
    }
    readInt(a, f);
    std::vector<uint64_t> b;
    arithReadFromStream(b, std::vector<uint64_t>(a, 1<<width), f);
  } else {
    readInt(th, f), readInt(l4h, f);
    readInt(p, f), readInt(width, f), readInt(sym, f), readInt(stator, f);
    readInt(bthh, f);
    // INFO << th << ' ' << l4h << ' ' << p << ' ' << width << ' ' << sym << ' ' << stator << ' ' << bthh << '\n';
    calculateMasks(bthh);
    int filterrows;
    readInt(filterrows, f);
    // INFO << filterrows << '\n';
    filters = std::vector<uint64_t>(filterrows);
    for (int i = 0; i < filterrows; i++) {
      readInt(filters[i], f);
    }
    // std::cerr << "hi" << f.fail()<<'\n';
    for(int s=0; s<2; s++) {
      int sz; readInt(sz, f);
      // std::cerr << "xhi" << f.fail()<<' '<<sz<<'\n';
      arithReadFromStream(leftborder[s], std::vector<uint64_t>(sz, 1<<p), f);
    }
    // std::cerr << "hi" << f.fail()<<'\n';
    int eis; readInt(eis, f);
    arithReadFromStream(exInitrow, std::vector<uint64_t>(eis, 1<<width), f);
  }
  uint64_t cksum;
  searchTree* tree = new searchTree;
  if(mode == "loadTree") cksum = tree->loadTree(f);
  else if(mode == "loadWorkunit") cksum = tree->loadWorkUnit(f);
  // else if(mode == "loadWorkunitResponse") cksum = tree->loadWorkUnitResponse(f);
  else assert(0);
  uint64_t expected_cksum; readInt(expected_cksum, f);
  if(mode == "loadTree") INFO << "Loaded search tree; checksum " << std::hex << cksum << std::dec << "\n";
  if (cksum != expected_cksum)
    throw std::runtime_error("Checksum mismatch: expected " + std::to_string(expected_cksum) + ", got " + std::to_string(cksum));
  // BENCHMARKEND(load)
  return tree;
}

void dumpf(std::ostream &f, std::string mode, const searchTree* tree, int onx = -1) {
  // BENCHMARK(dump)
  writeInt(th, f); writeInt(l4h, f);
  writeInt(p, f); writeInt(width, f); writeInt(sym, f); writeInt(stator, f);
  writeInt(bthh, f);
  writeInt(filters.size(), f);
  // WARN << sym << ' '<< stator << ' ' << bthh << '\n';
  for (auto i : filters)
    writeInt(i, f);
  for(int s=0; s<2; s++) {
    writeInt(leftborder[s].size(), f);
    arithWriteToStream(leftborder[s], std::vector<uint64_t>(leftborder[s].size(), 1<<p), f);
  }
  writeInt(exInitrow.size(), f);
  arithWriteToStream(exInitrow, std::vector<uint64_t>(exInitrow.size(), 1<<width), f);
  uint64_t cksum;
  if(mode == "dumpTree") cksum = tree->dumpTree(f);
  else if(mode == "dumpWorkunit") cksum = tree->dumpWorkUnit(f, onx);
  // else if(mode == "dumpWorkunitResponse") cksum = tree->dumpWorkUnitResponse(f);
  else assert(0);
  writeInt(cksum, f);
  f.flush();
  // BENCHMARKEND(dump)
  if(mode == "dumpTree") INFO << "Saved search tree; checksum " << std::hex << cksum << std::dec << "\n";
}
